//
//  GZCLoopBannelDefines.swift
//  GZCLoopBannel
//
//  Created by Guo ZhongCheng on 2020/10/28.
//

import GZCList

// 自定义Item可以通过实现此协议来获取是否成为展示对象
@objc public protocol GZCBannelItemType {
    /// 成为第一展示对象
    @objc func becomeFirstDisplay()
    /// 失去第一展示对象
    @objc func lostFirstDisplay()
}

// 自定义PageControl需要实现的协议
public protocol GZCPageControlProtocol: UIView {
    /// 设置总页数
    var totalPages: Int { get set }
    
    /// 更新滚动的进度
    func updateProgress(_ progress: CGFloat)
    
    /// 根据页数计算并返回control的size
    /// - Parameters:
    ///   - bannelViewSize: 总容器的大小
    func controlSize(_ bannelViewSize: CGSize) -> CGSize
}

// pageControl的位置
public enum GZCPageControlPosition {
    case topCenter
    case topLeft
    case topRight
    case bottomCenter
    case bottomLeft
    case bottomRight
}

/// pageControl样式
public enum GZCPageControlStyle {
    // 无
    case none
    
    // 圆形
    case system(tintColor: UIColor,currentColor: UIColor)
    
    // 未选中时空心圆，选中时动画填满
    case fill(tintColor: UIColor,indicatorPadding: CGFloat = 8, indicatorRadius: CGFloat = 4)
    
    // 横线形
    case pill(size: CGSize,
              indicatorPadding: CGFloat,
              activeColor: UIColor,
              unActiveColor: UIColor)
    
    // 选中横线占两个位置
    case longActivite(
            dotHeight: CGFloat = 4,
            dotUnActiviteWidth: CGFloat = 4,
            dotSpace: CGFloat = 4,
            dotCornerRadius: CGFloat = 2,
            dotUnActiviteColor: UIColor = .white,
            dotActiviteColor: UIColor = .white,
            backgroundColor: UIColor = UIColor.black.withAlphaComponent(0.3),
            backgroundCornerRadius: CGFloat = 4,
            contentInsets: UIEdgeInsets = UIEdgeInsets(top: 2, left: 3, bottom: 2, right: 3)
         )
    
    // 切换时带有拉伸动画
    case snake(
            activeColor: UIColor = .systemBlue,
            unActiveColor: UIColor = .gray,
            indicatorPadding: CGFloat = 5,
            indicatorRadius: CGFloat = 5
         )
    
    // 图片
    case image(
            activeImage: UIImage,
            unActiveImage: UIImage,
            dotActiveSize: CGSize = CGSize(width: 10, height: 10),
            dotUnActiveSize: CGSize = CGSize(width: 10, height: 10),
            dotSpace: CGFloat = 5)
    
    // 选中位置显示带数字的label，其他显示横杠
    case number(
            dotFont: UIFont,
            dotTextColor: UIColor,
            dotUnActiveColor: UIColor,
            dotActiveBgColor: UIColor,
            dotUnActiveSize: CGSize,
            dotActiveSize: CGSize,
            dotSpace: CGFloat)
    
    // 自定义
    case custom(_ customControl: GZCPageControlProtocol)
}

/// GZCLoopBannelViewDelegate
@objc public protocol GZCLoopBannelViewDelegate: class {
    @objc optional func loopBannelView(_ loopBannelView: GZCLoopBannelView, didScrollTo index: NSInteger)
}
