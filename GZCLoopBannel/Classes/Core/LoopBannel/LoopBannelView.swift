//
//  LoopBannelView.swift
//  GZCLoopBannel
//
//  Created by Guo ZhongCheng on 2020/10/26.
//

@_exported import GZCList

import GZCList
import GZCTimerHelper

public protocol LoopBannelViewProgressDelegate: NSObject {
    /// 滚动进度改变回调
    func progressChanged(_ progress: CGFloat)
}

public class LoopBannelView: FormCollectionView {
    public override var scrollDirection: UICollectionView.ScrollDirection {
        didSet {
            layout.scrollDirection = scrollDirection
            layout.invalidateLayout()
        }
    }
    
    /// 进度代理
    public weak var progressDelegate: LoopBannelViewProgressDelegate?
    
    // 轮播图配置
    public var bannelConfig: LoopBannelConfig = LoopBannelConfig() {
        didSet {
            upadateConfig()
        }
    }
    
    /// 获取当前进度(如： 1.05 表示第一个row 正朝着第二row 滚动，已滚动了 itemWidth*0.05 的距离)
    var _tempProgress: CGFloat = 0
    public var currentProgress: CGFloat {
        let indexpath = currentIndexPath
        guard
            let cell = self.cellForItem(at: indexpath)
        else {
            return _tempProgress
        }
        var centerPoint = CGPoint(x: self.contentOffset.x + self.center.x, y: self.contentOffset.y + self.center.y)
        var progress: CGFloat = CGFloat(indexpath.row)
        if scrollDirection == .horizontal {
            centerPoint.x += bannelConfig.centerOffset
            progress += (centerPoint.x - cell.center.x) / bannelConfig.itemWidth
        } else {
            centerPoint.y += bannelConfig.centerOffset
            progress += (centerPoint.y - cell.center.y) / bannelConfig.itemWidth
        }
        _tempProgress = progress
        return progress
    }
    
    /// 当前位置
    var _tempIndexPath: IndexPath = IndexPath(row: 0, section: 0)
    var currentIndexPath: IndexPath {
        var targetPoint = CGPoint(x: self.contentOffset.x + self.center.x, y: self.contentOffset.y + self.center.y)
        if scrollDirection == .horizontal {
            targetPoint.x += bannelConfig.centerOffset
        } else {
            targetPoint.y += bannelConfig.centerOffset
        }
        if let indexPath = self.indexPathForItem(at: targetPoint) {
            _tempIndexPath = indexPath
            return indexPath
        }
        return _tempIndexPath
    }
    
    /// 布局
    let layout = LoopBannelLayout()
    /// 计时器
    var timer: GZCTimerHelper?
    /// 当前间隔时间
    var currentTimeInterval: Double = 4
    
    public override func didMoveToWindow() {
        super.didMoveToWindow()
        /// 延迟刷新，等待reloaddata
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.05) { [weak self] in
            self?.autoScrollToCenterSection()
        }
    }
    
    public override func defaultSettings() {
        super.defaultSettings()
        self.backgroundColor = .clear
        self.itemSpace = 0
        self.lineSpace = 10
        self.scrollDirection = .horizontal
        self.arrangement = .custom(layout)
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false
        self.delegate = self
        self.dataSource = self
        self.clipsToBounds = false
        self.decelerationRate = .fast
    }
    
    func transformIndexPath(_ indexPath: IndexPath) -> IndexPath {
        return IndexPath(row: indexPath.row, section: transformSection(indexPath.section))
    }
    
    func transformSection(_ section: Int) -> Int {
        if form.count == 1 {
            return 0
        }
        return section % (form.count - 1)
    }
    
    public override var bounds: CGRect {
        didSet {
            transformContentInset()
        }
    }
    
    func transformContentInset() {
        if scrollDirection == .horizontal {
            let newLeft = (bounds.width - bannelConfig.itemWidth) * 0.5 + bannelConfig.centerOffset + lineSpace
            let newRight = (bounds.width - bannelConfig.itemWidth) * 0.5 - bannelConfig.centerOffset - lineSpace
            if abs(newLeft - contentInset.left) > 1 || abs(newRight - contentInset.right) > 1 {
                contentInset.left = newLeft
                contentInset.right = newRight
                layout.invalidateLayout()
            }
        } else {
            let newTop = (bounds.height - bannelConfig.itemWidth) * 0.5 + bannelConfig.centerOffset + lineSpace
            let newBottom = (bounds.height - bannelConfig.itemWidth) * 0.5 - bannelConfig.centerOffset - lineSpace
            if abs(newTop - contentInset.top) > 1 || abs(newBottom - contentInset.bottom) > 1 {
                contentInset.top = newTop
                contentInset.bottom = newBottom
                layout.invalidateLayout()
            }
        }
    }
    
    func beginAutoScrollIfNeeded() {
        if bannelConfig.autoScroll, form.allRows.count > 0 {
            if timer == nil {
                timer = GZCTimerHelper(bannelConfig.autoScrollTimeInterval)
                currentTimeInterval = bannelConfig.autoScrollTimeInterval
            }
            timer?.addObserver(self)
        } else {
            stopAutoScroll()
        }
    }
    
    func stopAutoScroll() {
        timer?.removeObserver(self)
        timer = nil
    }
    
    func upadateConfig() {
        /// 数值校验
        let scale = min(1.0, max(0, bannelConfig.minScale))
        bannelConfig.minScale = scale
        let width = max(0.01, bannelConfig.itemWidth)
        bannelConfig.itemWidth = width
        
        /// 设置layout
        layout.minScaleValue = bannelConfig.minScale
        layout.minOffset = bannelConfig.minOffset
        layout.centerOffset = bannelConfig.centerOffset
        scrollDirection = bannelConfig.scrollDirection
        lineSpace = bannelConfig.itemSpace
        reloadData()
        layout.invalidateLayout()
        if currentTimeInterval != bannelConfig.autoScrollTimeInterval {
            stopAutoScroll()
        }
        beginAutoScrollIfNeeded()
    }
}

extension LoopBannelView {
    public override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        upadateConfig()
        if newWindow != nil {
            beginAutoScrollIfNeeded()
        } else {
            stopAutoScroll()
        }
    }
}

// MARK:- TimerHelper
extension LoopBannelView: GZCTimerHelperDelegate {
    public func timerImplement() {
        DispatchQueue.runOnMain {
            self.scrollToIndex(self.nextIndexPath())
        }
    }
    
    /// 获取下一个Row的位置
    func nextIndexPath() -> IndexPath {
        let current = currentIndexPath
        var targetRow = current.row + 1
        var targetSection = current.section
        if form[transformSection(current.section)].allRows.count <= targetRow {
            if bannelConfig.infiniteLoop == false {
                // 已滚动到最后一张 停止计时
                stopAutoScroll()
                targetRow = current.row
            } else {
                targetRow = 0
                targetSection = targetSection + 1
            }
        }
        return IndexPath(row: targetRow, section: targetSection)
    }
    
    /// 获取上一个Row的位置
    func lastIndexPath() -> IndexPath {
        let current = currentIndexPath
        var targetRow = current.row - 1
        var targetSection = current.section
        if targetRow < 0 {
            if bannelConfig.infiniteLoop {
                targetRow = form[transformSection(current.section)].allRows.count - 1
                targetSection = targetSection - 1
            } else {
                targetRow = 0
            }
        }
        return IndexPath(row: targetRow, section: targetSection)
    }
    
    /// 滚动到指定位置
    func scrollToIndex(_ indexPath: IndexPath, animation: Bool = true) {
        let targetPoint = layout.targetPositionToSelectIndexPath(at: indexPath)
        DispatchQueue.runOnMain {
            self.setContentOffset(targetPoint, animated: animation)
        }
    }
}

// MARK:- UICollectionViewDataSource
extension LoopBannelView: UICollectionViewDataSource {
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        if bannelConfig.infiniteLoop {
            return form.count * 100
        }
        return form.count
    }

    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if form.count == 0 {
            return 0
        }
        return form[transformSection(section)].count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return handler.collectionView(collectionView, cellForItemAt: transformIndexPath(indexPath))
    }
    
    public func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        return handler.collectionView(collectionView, viewForSupplementaryElementOfKind: kind, at: transformIndexPath(indexPath))
    }
    
    // 设置是否可以移动
    public func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return handler.collectionView(collectionView, canMoveItemAt: transformIndexPath(indexPath))
    }
    
    // 移动后交换数据
    public func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        return handler.collectionView(collectionView, moveItemAt: sourceIndexPath, to: destinationIndexPath)
    }
}

// MARK:- UICollectionViewDelegateFlowLayout
extension LoopBannelView: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width = bannelConfig.itemWidth
        var height = bannelConfig.itemWidth
        if scrollDirection == .horizontal {
            height = self.frame.height - contentInset.top - contentInset.bottom
        } else {
            width = self.frame.width - contentInset.left - contentInset.right
        }
        return CGSize(width: width, height: height)
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        var inset = handler.collectionView(collectionView, layout: collectionViewLayout, insetForSectionAt: transformSection(section))
        if scrollDirection == .vertical {
            inset.bottom += lineSpace
        } else {
            inset.right += lineSpace
        }
        return inset
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return handler.collectionView(collectionView, layout: collectionViewLayout, minimumLineSpacingForSectionAt: transformSection(section))
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return handler.collectionView(collectionView, layout: collectionViewLayout, minimumInteritemSpacingForSectionAt: transformSection(section))
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return handler.collectionView(collectionView, layout: collectionViewLayout, referenceSizeForHeaderInSection: transformSection(section))
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return handler.collectionView(collectionView, layout: collectionViewLayout, referenceSizeForFooterInSection: transformSection(section))
    }
}

extension LoopBannelView: UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        handler.collectionView(collectionView, didHighlightItemAt: transformIndexPath(indexPath))
    }
    
    public func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        handler.collectionView(collectionView, didUnhighlightItemAt: transformIndexPath(indexPath))
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let targetPoint = layout.targetPositionToSelectIndexPath(at: currentIndexPath)
        if ceil(targetPoint.x) == ceil(collectionView.contentOffset.x),
           ceil(targetPoint.y) == ceil(collectionView.contentOffset.y) {
            handler.collectionView(collectionView, didSelectItemAt: transformIndexPath(indexPath))
        }
        scrollToIndex(indexPath)
    }
    
    // 可变Section添加row
    public func collectionView(_ collectionView: UICollectionView, addRowAt indexPath: IndexPath) {
        handler.collectionView(collectionView, addRowAt: transformIndexPath(indexPath))
    }
    
    public func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        handler.collectionView(collectionView, willDisplay: cell, forItemAt: transformIndexPath(indexPath))
    }

    public func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        handler.collectionView(collectionView, didEndDisplaying: cell, forItemAt: transformIndexPath(indexPath))
    }
    
    // MARK: UIScrollViewDelegate
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        progressDelegate?.progressChanged(currentProgress)
    }

    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        stopAutoScroll()
    }
    
    public func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        /// 当前位置
        let offset = contentOffset
        /// 触发翻页的速度
        let pageVelocity: CGFloat = 0.8
        /// 判断滚动终点
        var targetIndexPath: IndexPath
        if scrollDirection == .horizontal {
            let targetOffset = layout.pointTargetContentOffset(
                forProposedContentOffset: offset,
                withScrollingVelocity: velocity
            )
            let dx = targetOffset.x - offset.x
            if dx < 0, velocity.x > pageVelocity {
                targetIndexPath = nextIndexPath()
            } else
            if dx > 0, velocity.x < -pageVelocity {
                targetIndexPath = lastIndexPath()
            } else {
                targetIndexPath = currentIndexPath
            }
        } else {
            let targetOffset = layout.pointTargetContentOffset(
                forProposedContentOffset: offset,
                withScrollingVelocity: velocity
            )
            let dy = targetOffset.y - offset.y
            if dy < 0, velocity.y > pageVelocity {
                targetIndexPath = nextIndexPath()
            } else
            if dy > 0, velocity.y < -pageVelocity {
                targetIndexPath = lastIndexPath()
            } else {
                targetIndexPath = currentIndexPath
            }
        }
        let targetPoint = layout.targetPositionToSelectIndexPath(at: targetIndexPath)
        if
            (
                !bannelConfig.infiniteLoop &&
                !((
                    targetIndexPath == IndexPath(row: 0, section: 0) &&
                    (velocity.x < 0 || velocity.y < 0)
                ) || (
                    targetIndexPath == IndexPath(row: form[form.allSections.count - 1].count - 1, section: form.allSections.count - 1) &&
                    (velocity.x > 0 || velocity.y > 0)
                ))
            ) || bannelConfig.infiniteLoop
        {
            /// 满足以下条件，不做特殊处理：
            /// 1、不是无限轮播
            /// 2、是第一个item且往后滑动
            ///    是或最后一个item且往前滑动
            if
                (targetPoint.x < targetContentOffset.pointee.x && velocity.x > 0) ||
                (targetPoint.x > targetContentOffset.pointee.x && velocity.x < 0) ||
                (targetPoint.y < targetContentOffset.pointee.y && velocity.y > 0) ||
                (targetPoint.y > targetContentOffset.pointee.y && velocity.y < 0)
            {
                /// 目标位置方向与滚动方向相反，使用滚动动画进行处理，避免跳动
                targetContentOffset.pointee = offset
                setContentOffset(targetPoint, animated: true)
                return
            }
        }
        targetContentOffset.pointee = targetPoint
    }

    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        progressDelegate?.progressChanged(currentProgress)
        beginAutoScrollIfNeeded()
    }

    public func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        progressDelegate?.progressChanged(currentProgress)
        beginAutoScrollIfNeeded()
    }
    
    func autoScrollToCenterSection() {
        if bannelConfig.infiniteLoop {
            let currentIndex = currentIndexPath
            if currentIndex.section < 2 || currentIndex.section > 5 {
                scrollToIndex(IndexPath(row: currentIndex.row, section: 3), animation: false)
            }
        } else {
            scrollToIndex(currentIndexPath)
        }
    }
}


