//
//  NumberPageControl.swift
//  GZCLoopBannel
//
//  Created by Guo ZhongCheng on 2020/10/28.
//

open class NumberPageControl: UIView {

    open var dotFont: UIFont = UIFont.systemFont(ofSize: 10) {
        didSet {
            for dot in dotArray {
                dot.font = dotFont
            }
        }
    }
    
    open var dotTextColor: UIColor = .white {
        didSet {
            for dot in dotArray {
                dot.textColor = dotTextColor
            }
        }
    }
    
    open var dotUnActiveColor: UIColor = .darkGray {
       didSet {
           updateDots()
       }
    }
    
    open var dotActiveBgColor: UIColor = .systemBlue {
       didSet {
           updateDots()
       }
    }

    
    open var dotUnActiveSize: CGSize = CGSize(width: 6, height: 3) {
       didSet {
           updateDots()
       }
    }
    open var dotActiveSize: CGSize = CGSize(width: 11, height: 11) {
       didSet {
           updateDots()
       }
    }
    
    open var dotSpace: CGFloat = 4 {
       didSet {
           updateDots()
       }
    }
    
    fileprivate var dotArray = [UILabel]()
    
    open var numberOfPages: Int = 0 {
        didSet {
            for view in dotArray {
                view.removeFromSuperview()
            }
            dotArray.removeAll()
            for _ in 0...numberOfPages-1 {
                dotArray.append(label())
            }
            updateDots()
        }
    }
    
    open var currentPage: Int = 0 {
        didSet {
            updateDots()
        }
    }
    
    func updateDots() {
        for (index, dot) in dotArray.enumerated() {
            if index == self.currentPage {
                dot.frame = CGRect.init(x: (dotUnActiveSize.width + dotSpace) * CGFloat(index), y: 0, width: dotActiveSize.width, height: dotActiveSize.height)
                dot.text = "\(index+1)"
                dot.backgroundColor = dotActiveBgColor
            } else {
                if index > self.currentPage {
                    dot.frame = CGRect.init(x: (dotUnActiveSize.width + dotSpace) * CGFloat(index - 1) + dotActiveSize.width + dotSpace, y: dotActiveSize.height - dotUnActiveSize.height, width: dotUnActiveSize.width, height: dotUnActiveSize.height)
                } else {
                    dot.frame = CGRect.init(x: (dotUnActiveSize.width + dotSpace) * CGFloat(index), y: dotActiveSize.height - dotUnActiveSize.height, width: dotUnActiveSize.width, height: dotUnActiveSize.height)
                }
                dot.text = ""
                dot.backgroundColor = dotUnActiveColor
            }
        }
    }
    
    fileprivate func label() -> UILabel {
        let dot: UILabel = UILabel()
        dot.font = dotFont
        dot.textColor = dotTextColor
        dot.textAlignment = .center
        addSubview(dot)
        return dot
    }
}

// MARK:- GZCPageControlProtocol
extension NumberPageControl: GZCPageControlProtocol {
    public var totalPages: Int {
        get {
            return numberOfPages
        }
        set {
            numberOfPages = newValue
        }
    }
    
    public func updateProgress(_ progress: CGFloat) {
        currentPage = Int(round(progress))
    }
    
    public func controlSize(_ bannelViewSize: CGSize) -> CGSize {
        return CGSize(width: dotActiveSize.width + dotSpace + (dotUnActiveSize.width + dotSpace) * CGFloat(numberOfPages - 1), height: dotActiveSize.height)
    }
}
