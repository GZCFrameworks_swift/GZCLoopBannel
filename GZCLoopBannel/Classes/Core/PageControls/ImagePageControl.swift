//
//  ImagePageControl.swift
//  GZCLoopBannel
//
//  Created by Guo ZhongCheng on 2020/10/26.
//

open class ImagePageControl: UIView {
    
    open var dotUnActiveImage: UIImage = UIImage()
    open var dotActiveImage: UIImage = UIImage()
    
    open var dotUnActiveSize: CGSize = CGSize(width: 10, height: 10) {
       didSet {
           updateDots()
       }
    }
    open var dotActiveSize: CGSize = CGSize(width: 10, height: 10) {
       didSet {
           updateDots()
       }
    }
    
    open var dotSpace: CGFloat = 5 {
       didSet {
           updateDots()
       }
    }
    
    open var numberOfPages: Int = 0 {
        didSet {
            for view in dotArray {
                view.removeFromSuperview()
            }
            dotArray.removeAll()
            for _ in 0...numberOfPages-1 {
                dotArray.append(imageView())
            }
            updateDots()
        }
    }
    
    open var currentPage: Int = 0 {
        didSet {
            updateDots()
        }
    }
    
    fileprivate var dotArray = [UIImageView]()
    
    func updateDots() {
        for (index, dot) in dotArray.enumerated() {
            if index == self.currentPage {
                dot.frame = CGRect.init(x: (dotUnActiveSize.width + dotSpace) * CGFloat(index), y: 0, width: dotActiveSize.width, height: dotActiveSize.height)
                dot.image = dotActiveImage
            } else {
                if index > self.currentPage {
                    dot.frame = CGRect.init(x: (dotUnActiveSize.width + dotSpace) * CGFloat(index - 1) + dotActiveSize.width + dotSpace, y: dotActiveSize.height - dotUnActiveSize.height, width: dotUnActiveSize.width, height: dotUnActiveSize.height)
                } else {
                    dot.frame = CGRect.init(x: (dotUnActiveSize.width + dotSpace) * CGFloat(index), y: dotActiveSize.height - dotUnActiveSize.height, width: dotUnActiveSize.width, height: dotUnActiveSize.height)
                }
                dot.image = dotUnActiveImage
            }
        }
    }
    
    fileprivate func imageView(forSubview view: UIView) -> UIImageView? {
        var dot: UIImageView?
        if let dotImageView = view as? UIImageView {
            dot = dotImageView
        } else {
            for foundView in view.subviews {
                if let imageView = foundView as? UIImageView {
                    dot = imageView
                    break
                }
            }
        }
        return dot
    }
    
    fileprivate func imageView() -> UIImageView {
        let dot: UIImageView = UIImageView()
        dot.contentMode = .scaleAspectFit
        addSubview(dot)
        return dot
    }
}

// MARK:- GZCPageControlProtocol
extension ImagePageControl: GZCPageControlProtocol {
    public var totalPages: Int {
        get {
            return numberOfPages
        }
        set {
            numberOfPages = newValue
        }
    }
    
    public func updateProgress(_ progress: CGFloat) {
        currentPage = Int(round(progress))
    }
    
    public func controlSize(_ bannelViewSize: CGSize) -> CGSize {
        return CGSize(width: 8 + 8 + (8 + 8) * CGFloat(numberOfPages - 1), height: 8)
    }
}
