//
//  LoopTableFormDemo.swift
//  GZCLoopBannel_Example
//
//  Created by Guo ZhongCheng on 2020/10/29.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import GZCList
import GZCLoopBannel
import Kingfisher

class LoopTableFormDemo: FormTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let section = TableSection()
        
        // 自定义header
        var headerProvider = TableHeaderFooterView<GZCLoopBannelView>(.callback { () -> GZCLoopBannelView in
            let bannelView = GZCLoopBannelView()
            var items = [ImageItem]()
            for _ in 0 ..< 10 {
                items += getRoundImageRow()
            }
            bannelView.items = items
            bannelView.backgroundColor = .white
            bannelView.bannelConfig.minScale = 0.9
            bannelView.bannelConfig.itemSpace = 15
            bannelView.bannelConfig.minOffset = -120 * (1 - bannelView.bannelConfig.minScale) * 0.5
            bannelView.bannelConfig.centerOffset = -22.5
            bannelView.bannelInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
            return bannelView
        })
        headerProvider.height = { 140 }
        section.header = headerProvider
        
        for _ in 0 ..< 500 {
            section
                <<< getRandomRow()
                <<< LineRow() { row in
                    row.lineColor = .clear
                    row.backgroundColor = .clear
                    row.cellHeight = 15
                }
        }
        
        form +++ section
    }
    
    func getRandomRow() -> GZCLoopBannelRow {
        return GZCLoopBannelRow() { row in
            var items = [ImageItem]()
            for _ in 0 ..< 10 {
                items += getRoundImageRow()
            }
            row.value = items
            row.cellHeight = getRandomHeight()
            row.pageControlConfig.pageControlStyle = getRandomPageControlStyle()
            row.bannelConfig.itemWidth = CGFloat(arc4random() % 50 + 300)
        }
    }
    
    
    deinit {
        /// 此控制器中会创建大量图片在内存中占用，可以使用此方法快速清除内存中的图片
        ImageCache.default.clearMemoryCache()
        print("----释放了————")
    }
}
