//
//  SystemPageControl.swift
//  GZCLoopBannel
//
//  Created by Guo ZhongCheng on 2020/10/28.
//

import Foundation

@objc extension UIPageControl: GZCPageControlProtocol {
    public var totalPages: Int {
        get {
            return numberOfPages
        }
        set {
            numberOfPages = newValue
        }
    }
    
    @objc public func controlSize(_ bannelViewSize: CGSize) -> CGSize {
        return CGSize(width: bannelViewSize.width, height: 10)
    }
    
    @objc public func updateProgress(_ progress: CGFloat) {
        currentPage = Int(round(progress))
    }
}
