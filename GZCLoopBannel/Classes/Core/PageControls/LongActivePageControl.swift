//
//  LongActivePageControl.swift
//  GZCLoopBannel
//
//  Created by Guo ZhongCheng on 2020/10/30.
//

import Foundation

open class LongActivePageControl: UIView, GZCPageControlProtocol {
    /// 四边间距
    open var contentInsets: UIEdgeInsets = UIEdgeInsets(top: 2, left: 3, bottom: 2, right: 3) {
        didSet {
            updateDots()
        }
    }
    
    /// 点的高度
    open var dotHeight: CGFloat = 4 {
       didSet {
           updateDots()
       }
    }
    
    /// 未选中点的长度
    open var dotUnActiviteWidth: CGFloat = 4 {
       didSet {
           updateDots()
       }
    }
    
    /// 选中点的长度
    open var dotActiviteWidth: CGFloat {
       return dotUnActiviteWidth * 2 + dotSpace
    }
    
    /// 点之间的间距
    open var dotSpace: CGFloat = 4 {
       didSet {
           updateDots()
       }
    }
    
    /// 点的圆角
    open var dotCornerRadius: CGFloat = 0 {
        didSet {
            for view in dotArray {
                view.layer.cornerRadius = dotCornerRadius
            }
            indicatorView.layer.cornerRadius = dotCornerRadius
        }
    }
    
    /// 未选中点的颜色
    open var dotUnActiviteColor: UIColor = .white {
        didSet {
            for view in dotArray {
                view.backgroundColor = dotUnActiviteColor
            }
        }
    }
    
    /// 选中点的颜色
    open var dotActiviteColor: UIColor = .white {
        didSet {
            indicatorView.backgroundColor = dotActiviteColor
        }
    }
    
    fileprivate var dotArray = [UIView]()
    fileprivate lazy var indicatorView = dotView(dotActiviteColor)
    
    func updateDots() {
        for (index, dot) in dotArray.enumerated() {
            dot.frame = CGRect.init(x: contentInsets.left + (dotUnActiviteWidth + dotSpace) * CGFloat(index), y: contentInsets.top, width: dotUnActiviteWidth, height: dotHeight)
        }
        indicatorView.frame = CGRect(x: contentInsets.left + CGFloat(self.totalPages) * (dotUnActiviteWidth + dotSpace), y: contentInsets.top, width: dotActiviteWidth, height: dotHeight)
    }
    
    fileprivate func updateActivePageIndicator(forProgress p: CGFloat) {
        let progress = min(CGFloat(self.totalPages - 1), max(0, p))
        indicatorView.frame = CGRect(x: contentInsets.left + progress * (dotUnActiviteWidth + dotSpace), y: contentInsets.top, width: dotActiviteWidth, height: dotHeight)
    }
    
    fileprivate func dotView(_ color: UIColor) -> UIView {
        let dot: UIView = UIView()
        dot.backgroundColor = color
        dot.layer.cornerRadius = dotCornerRadius
        dot.clipsToBounds = true
        addSubview(dot)
        return dot
    }
    
    
    // MARK:- GZCPageControlProtocol
    public var totalPages: Int = 0 {
        didSet {
            for view in dotArray {
                view.removeFromSuperview()
            }
            dotArray.removeAll()
            for _ in 0...totalPages {
                dotArray.append(dotView(dotUnActiviteColor))
            }
            self.bringSubviewToFront(indicatorView)
            updateDots()
        }
    }
    
    public func updateProgress(_ progress: CGFloat) {
        updateActivePageIndicator(forProgress: progress)
    }
    
    open func controlSize(_ bannelViewSize: CGSize) -> CGSize {
        return CGSize(
            width: contentInsets.left + contentInsets.right + (dotSpace + dotUnActiviteWidth) * CGFloat(totalPages - 1) + dotActiviteWidth,
            height: contentInsets.top + contentInsets.bottom + dotHeight
        )
    }
}
