//
//  LoopCollectionFormDemo.swift
//  GZCLoopBannel_Example
//
//  Created by Guo ZhongCheng on 2020/10/30.
//  Copyright © 2020 CocoaPods. All rights reserved.
//


import GZCList
import GZCLoopBannel
import Kingfisher

class LoopCollectionFormDemo: FormCollectionViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.arrangement = .flow
        
        let section = CollectionSection()
        section.column = 1
        // 自定义header
        var headerProvider = CollectionHeaderFooterView<GZCLoopBannelCollectionHeader>.init { (view) in
            var items = [ImageItem]()
            for _ in 0 ..< 10 {
                items += getRoundImageRow()
            }
            view.bannelView.items = items
            view.bannelView.backgroundColor = .white
            view.bannelView.bannelConfig.minScale = 0.9
            view.bannelView.bannelConfig.itemSpace = 15
            view.bannelView.bannelConfig.minOffset = -120 * (1 - view.bannelView.bannelConfig.minScale) * 0.5
            view.bannelView.bannelConfig.centerOffset = -22.5
            view.bannelView.bannelInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        }
        headerProvider.height = { 140 }
        headerProvider.shouldSuspension = true
        section.header = headerProvider
        
        for _ in 0 ..< 500 {
            section
                <<< getRandomRow()
                <<< LineItem() { row in
                    row.lineColor = .clear
                    row.backgroundColor = .clear
                    row.lineWidth = 15
                }
        }
        
        form +++ section
    }
    
    func getRandomRow() -> GZCLoopBannelItem {
        return GZCLoopBannelItem() {[weak self] row in
            guard let strongSelf = self else {
                return
            }
            var items = [ImageItem]()
            for _ in 0 ..< 10 {
                items += getRoundImageRow()
            }
            row.value = items
            row.aspectRatio = CGSize(width: strongSelf.view.frame.width, height: getRandomHeight())
            row.pageControlConfig.pageControlStyle = getRandomPageControlStyle()
            row.bannelConfig.itemWidth = CGFloat(arc4random() % 50 + 300)
        }
    }
    
    
    deinit {
        /// 此控制器中会创建大量图片在内存中占用，可以使用此方法快速清除内存中的图片
        ImageCache.default.clearMemoryCache()
        print("----释放了————")
    }
}
