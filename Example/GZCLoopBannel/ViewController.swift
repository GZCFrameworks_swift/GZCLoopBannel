//
//  ViewController.swift
//  GZCLoopBannel
//
//  Created by Guo ZhongCheng on 10/26/2020.
//  Copyright (c) 2020 Guo ZhongCheng. All rights reserved.
//

import GZCLoopBannel
import GZCList

class ViewController: FormTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        form +++
            ButtonRow("轮播内容属性设置").onCellSelection({[weak self] (c, r) in
                let demo = BannelPropertyDemo()
                demo.title = "轮播内容属性设置"
                self?.show(demo, sender: nil)
            })
            <<< ButtonRow("PageControl样式设置").onCellSelection({[weak self] (c, r) in
                let demo = PageControlDemo()
                demo.title = "PageControl样式设置"
                self?.show(demo, sender: nil)
            })
            
            <<< LineRow() { row in
                row.cellHeight = 15
            }
        
            <<< ButtonRow("横向轮播").onCellSelection({[weak self] (c, r) in
                let demo = LoopBannelViewDemo()
                demo.title = "横向轮播"
                self?.show(demo, sender: nil)
            })
            <<< ButtonRow("纵向轮播").onCellSelection({[weak self] (c, r) in
                let demo = LoopBannelViewDemo()
                demo.scrollDirection = .vertical
                demo.title = "纵向轮播"
                self?.show(demo, sender: nil)
            })
        
            <<< LineRow() { row in
                row.cellHeight = 15
            }
        
            <<< ButtonRow("FormTableView使用GZCLoopBannelRow").onCellSelection({[weak self] (c, r) in
                let demo = LoopTableFormDemo()
                demo.title = "GZCLoopBannelRow与Header"
                self?.show(demo, sender: nil)
            })
            <<< ButtonRow("FormCollectionView使用GZCLoopBannelItem").onCellSelection({[weak self] (c, r) in
                let demo = LoopCollectionFormDemo()
                demo.title = "GZCLoopBannelItem与Header"
                self?.show(demo, sender: nil)
            })
        
    }
    
    
}

