//
//  DispatchQueueExtensions.swift
//  GZCExtends
//
//  Created by Guo ZhongCheng on 2020/10/20.
//

@_exported import SwifterSwift

// MARK: - Methods
public extension DispatchQueue {
    
    /// 延迟执行（主线程中）
    /// - Parameters:
    ///   - complate: 执行方法
    ///   - after: 延迟时间
    static func runOnMain(after: Double = 0.0,_ complate: @escaping () -> Void) {
        if after == 0.0 {
            if isMainQueue {
                complate()
            } else {
                DispatchQueue.main.async {
                    complate()
                }
            }
        } else {
            let deadline = DispatchTime.now() + after
            DispatchQueue.global().asyncAfter(deadline: deadline) {
                DispatchQueue.main.async {
                    complate()
                }
            }
        }
    }
}
