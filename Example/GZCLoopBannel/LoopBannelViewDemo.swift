//
//  LoopBannelViewDemo.swift
//  GZCLoopBannel_Example
//
//  Created by Guo ZhongCheng on 2020/10/29.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import GZCLoopBannel

class LoopBannelViewDemo: UIViewController {
    
    var scrollDirection: UICollectionView.ScrollDirection = .horizontal
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        let system = systemBannel()
//        system.autoScroll = false
        self.view.addSubview(system)
        system.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(150)
        }

        var numberItemWidth = self.view.frame.width - 65
        var numberItemHeight = numberItemWidth * 177.0 / 310.0
        if scrollDirection == .vertical {
            let temp = numberItemWidth
            numberItemWidth = numberItemHeight
            numberItemHeight = temp
        }
        let number = numberBannel(numberItemWidth, numberItemHeight)
//        number.infiniteLoop = false
        self.view.addSubview(number)
        number.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(system.snp.bottom).offset(10)
            make.height.equalTo(scrollDirection == .horizontal ? numberItemHeight : numberItemWidth + 60)
        }
        
        let imgBannel = imageBannel(numberItemWidth, numberItemHeight)
//        imgBannel.autoScroll = false
        self.view.addSubview(imgBannel)
        imgBannel.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(number.snp.bottom).offset(10)
            make.height.equalTo(scrollDirection == .horizontal ? numberItemHeight : numberItemWidth)
        }
        
        system.bannelConfig.scrollDirection = scrollDirection
        number.bannelConfig.scrollDirection = scrollDirection
        imgBannel.bannelConfig.scrollDirection = scrollDirection
    }
    
    func systemBannel() -> GZCLoopBannelView {
        let bannelView = GZCLoopBannelView()
        bannelView.pageControlConfig.pageControlStyle = .system(tintColor: .gray, currentColor: .systemBlue)
        var items = [ImageItem]()
        for _ in 0 ..< 10 {
            items += getImageRow()
        }
        bannelView.items = items
        
        bannelView.bannelConfig.itemWidth = self.scrollDirection == .horizontal ? self.view.frame.width : 150
        bannelView.bannelConfig.itemSpace = 0
        bannelView.bannelConfig.minScale = 1.0
        bannelView.clipsToBounds = true
        return bannelView
    }
    
    func numberBannel(_ itemWidth: CGFloat, _ itemHeight: CGFloat) -> GZCLoopBannelView {
        let bannelView = GZCLoopBannelView()
        bannelView.pageControlConfig.pageControlStyle = .number(
            dotFont: .systemFont(ofSize: 13),
            dotTextColor: .white,
            dotUnActiveColor: .gray,
            dotActiveBgColor: .systemBlue,
            dotUnActiveSize: CGSize(width: 10, height: 3),
            dotActiveSize: CGSize(width: 15, height: 20),
            dotSpace: 5
        )
        bannelView.pageControlConfig.pageControlPosition = .bottomLeft
        bannelView.pageControlConfig.pageControlInsets = UIEdgeInsets(top: 0, left: 20, bottom: 5, right: 0)
        
        var items = [ImageItem]()
        for _ in 0 ..< 10 {
            items += getRoundImageRow()
        }
        bannelView.items = items
        bannelView.bannelConfig.minScale = 0.9
        bannelView.bannelConfig.itemSpace = 15
        if scrollDirection == .horizontal {
            bannelView.bannelConfig.minOffset = -itemHeight * (1 - bannelView.bannelConfig.minScale) * 0.5
            bannelView.bannelConfig.centerOffset = -itemWidth * (1 - bannelView.bannelConfig.minScale) * 0.5
        } else {
            bannelView.bannelConfig.minOffset = -itemWidth * (1 - bannelView.bannelConfig.minScale) * 0.5
            bannelView.bannelConfig.centerOffset = -itemHeight * (1 - bannelView.bannelConfig.minScale) * 0.5
        }
        bannelView.bannelConfig.itemWidth = itemWidth
        bannelView.clipsToBounds = true
        return bannelView
    }
    
    func imageBannel(_ itemWidth: CGFloat, _ itemHeight: CGFloat) -> GZCLoopBannelView {
        let bannelView = GZCLoopBannelView()
        bannelView.pageControlConfig.pageControlStyle = .image(activeImage: UIImage(named: "selected")!, unActiveImage: UIImage(named: "unselected")!, dotSpace: 5)
        
        var items = [ImageItem]()
        for _ in 0 ..< 10 {
            items += getRoundImageRow()
        }
        bannelView.items = items
        bannelView.bannelConfig.minScale = 0.85
        bannelView.bannelConfig.itemSpace = 20
        bannelView.bannelConfig.itemWidth = itemWidth
        bannelView.clipsToBounds = true
        return bannelView
    }
    
    deinit {
        print("----释放了————")
    }
}

