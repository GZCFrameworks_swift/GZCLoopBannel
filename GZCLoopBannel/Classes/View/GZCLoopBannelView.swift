//
//  GZCLoopBannelView.swift
//  GZCLoopBannel
//
//  Created by Guo ZhongCheng on 2020/10/28.
//

import GZCList

public class GZCLoopBannelView: UIView {
    // MARK:- 公开属性
    // 轮播图配置
    public var bannelConfig: LoopBannelConfig {
        get {
            return bannelView.bannelConfig
        }
        set {
            bannelView.bannelConfig = newValue
            bannelInsets = newValue.bannelInsets
        }
    }
    // pageControl配置
    public var pageControlConfig: PageControlConfig = PageControlConfig() {
        didSet {
            setupPageControl()
        }
    }
    
    /// 轮播图与self的间距（默认为0）
    public var bannelInsets: UIEdgeInsets = .zero {
        didSet {
            bannelView.snp.updateConstraints { (make) in
                make.edges.equalTo(bannelInsets)
            }
        }
    }
    /// 轮播的Item数组
    public var items = [CollectionItem]() {
        didSet {
            let section = CollectionSection()
            section.append(contentsOf: items)
            bannelView.form >>> [section]
            setupPageControl()
        }
    }
    
    /// 开始轮播
    public func beginLoop() {
        bannelView.beginAutoScrollIfNeeded()
    }
    
    public func stopLoop() {
        bannelView.stopAutoScroll()
    }
    
    // MARK:- 私有属性
    var bannelView = LoopBannelView()
    var pageControl: GZCPageControlProtocol?
    var currentIndex: Int = 0
    
    // MARK:- 初始化
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupBannelView()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupBannelView()
    }
    
    /// 更新布局
    public override var bounds: CGRect {
        didSet {
            updateLayout()
        }
    }
}

extension GZCLoopBannelView: LoopBannelViewProgressDelegate {
    public func progressChanged(_ progress: CGFloat) {
        pageControl?.updateProgress(progress)
        if progress < 0 {
            if currentIndex != items.count - 1 {
                if let item = items[currentIndex] as? GZCBannelItemType {
                    item.lostFirstDisplay()
                }
                currentIndex = items.count - 1
                if let item = items.last as? GZCBannelItemType {
                    item.becomeFirstDisplay()
                }
            }
            return
        }
        let targetIndex = Int(progress)
        if currentIndex != targetIndex {
            if let item = items[currentIndex] as? GZCBannelItemType {
                item.lostFirstDisplay()
            }
            currentIndex = targetIndex % items.count
            if let item = items[currentIndex] as? GZCBannelItemType {
                item.becomeFirstDisplay()
            }
        }
    }
}

// MARK:- UI设置
extension GZCLoopBannelView {
    /// 更新frame
    func updateLayout() {
        if let controlSize = pageControl?.controlSize(self.bounds.size) {
            var x: CGFloat = 0
            var y: CGFloat = 0
            switch pageControlConfig.pageControlPosition {
                case .topLeft:
                    x = pageControlConfig.pageControlInsets.left
                    y = pageControlConfig.pageControlInsets.top
                case.topRight:
                    x = self.bounds.width - controlSize.width - pageControlConfig.pageControlInsets.right
                    y = pageControlConfig.pageControlInsets.top
                case .topCenter:
                    x = (self.bounds.size.width - controlSize.width) * 0.5 + pageControlConfig.pageControlInsets.left - pageControlConfig.pageControlInsets.right
                    y = pageControlConfig.pageControlInsets.top
                case .bottomCenter:
                    x = (self.bounds.size.width - controlSize.width) * 0.5 + pageControlConfig.pageControlInsets.left - pageControlConfig.pageControlInsets.right
                    y = self.bounds.size.height - controlSize.height - pageControlConfig.pageControlInsets.bottom
                case .bottomLeft:
                    x = pageControlConfig.pageControlInsets.left
                    y = self.bounds.size.height - controlSize.height - pageControlConfig.pageControlInsets.bottom
                case .bottomRight:
                    x = self.bounds.width - controlSize.width - pageControlConfig.pageControlInsets.right
                    y = self.bounds.size.height - controlSize.height - pageControlConfig.pageControlInsets.bottom
            }
            pageControl?.frame = CGRect.init( origin: CGPoint(x: x, y: y), size: controlSize)
        }
    }
    
    /// 添加轮播图
    func setupBannelView() {
        bannelView.progressDelegate = self
        self.addSubview(bannelView)
        bannelView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    /// 添加PageControl
    func setupPageControl() {
        
        // 重新添加
        pageControl?.removeFromSuperview()
        
        if items.count <= 1 {
            return
        }
        
        switch pageControlConfig.pageControlStyle {
            case .none:
                break
            case .system(let tintColor, let currentColor):
                let control = UIPageControl()
                control.pageIndicatorTintColor = tintColor
                control.currentPageIndicatorTintColor = currentColor
                pageControl = control
            case .fill(let tintColor, let indicatorPadding, let indicatorRadius):
                let control = FilledPageControl(frame: CGRect.zero)
                control.tintColor = tintColor
                control.indicatorPadding = indicatorPadding
                control.indicatorRadius = indicatorRadius
                pageControl = control
            case .pill(let size,let indicatorPadding, let activeColor, let unActiveColor):
                let control = PillPageControl(frame: CGRect.zero)
                control.indicatorPadding = indicatorPadding
                control.activeTint = activeColor
                control.inactiveTint = unActiveColor
                control.pillSize = size
                pageControl = control
            case .longActivite(let dotHeight, let dotUnActiviteWidth, let dotSpace, let dotCornerRadius, let dotUnActiviteColor, let dotActiviteColor, let backgroundColor, let backgroundCornerRadius, let contentInsets):
                let control = LongActivePageControl()
                control.dotHeight = dotHeight
                control.dotUnActiviteWidth = dotUnActiviteWidth
                control.dotSpace = dotSpace
                control.dotCornerRadius = dotCornerRadius
                control.dotUnActiviteColor = dotUnActiviteColor
                control.dotActiviteColor = dotActiviteColor
                control.contentInsets = contentInsets
                control.backgroundColor = backgroundColor
                control.layer.cornerRadius = backgroundCornerRadius
                control.clipsToBounds = true
                pageControl = control
            case .snake(let activeColor, let unActiveColor, let indicatorPadding, let indicatorRadius):
                let control = SnakePageControl(frame: CGRect.zero)
                control.activeTint = activeColor
                control.indicatorPadding = indicatorPadding
                control.indicatorRadius = indicatorRadius
                control.inactiveTint = unActiveColor
                pageControl = control
            case .image(let activeImage, let unActiveImage, let dotActiveSize, let dotUnActiveSize, let dotSpace):
                let control = ImagePageControl()
                control.dotActiveImage = activeImage
                control.dotUnActiveImage = unActiveImage
                control.dotUnActiveSize = dotUnActiveSize
                control.dotActiveSize = dotActiveSize
                control.dotSpace = dotSpace
                pageControl = control
            case .number(let dotFont, let dotTextColor, let dotUnActiveColor, let dotActiveBgColor, let dotUnActiveSize, let dotActiveSize, let dotSpace):
                let control = NumberPageControl()
                control.dotFont = dotFont
                control.dotTextColor = dotTextColor
                control.dotUnActiveColor = dotUnActiveColor
                control.dotActiveBgColor = dotActiveBgColor
                control.dotUnActiveSize = dotUnActiveSize
                control.dotActiveSize = dotActiveSize
                control.dotSpace = dotSpace
                pageControl = control
            case .custom(let control):
                pageControl = control
        }
        if pageControl != nil {
            pageControl!.totalPages = self.items.count
            self.addSubview(pageControl!)
        }
        
        updateLayout()
        
        pageControl?.updateProgress(CGFloat(currentIndex))
    }
}
