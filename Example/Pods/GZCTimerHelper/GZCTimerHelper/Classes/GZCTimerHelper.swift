//
//  GZCTimerHelper.swift
//  GZCTimerHelper
//
//  Created by Guo ZhongCheng on 2020/10/26.
//

import Foundation

@objc public protocol GZCTimerHelperDelegate {
    func timerImplement()
}

@objc public class GZCTimerHelper: NSObject {
    // 单例
    @objc public static let shared = GZCTimerHelper()
    private override init() {}
    // 创建实例
    public convenience init(_ timeInterval: TimeInterval = 0.5) {
        self.init()
        self.timeInterval = timeInterval
    }
    // 配置间隔时间
    public var timeInterval: TimeInterval = 0.5
    
    /// 异步线程数量控制（最多10个）
    let runSemaphore = DispatchSemaphore(value: 10)
    
    /// 监听的对象实例集合
    var observers = NSHashTable<AnyObject>.weakObjects()
    
    /// 使用的timer
    var _timer: DispatchSourceTimer?
    var timer: DispatchSourceTimer {
        if _timer == nil {
            _timer = DispatchSource.makeTimerSource(flags: .strict, queue: DispatchQueue(label: "com.gzc.timerHelper\(timeInterval)"))
            _timer?.schedule(deadline: .now()+timeInterval, repeating: timeInterval)
            _timer?.setEventHandler(handler: { [weak self] in
                self?.timerAction()
            })
        }
        return _timer!
    }
    
    @objc func timerAction() {
        if observers.allObjects.count == 0 {
            timer.cancel()
            _timer = nil
            return
        }
        let copyObservers = observers.copy() as! NSHashTable<AnyObject>
        for observer in copyObservers.allObjects {
            if let d = observer as? GZCTimerHelperDelegate {
                DispatchQueue.global().async { [weak self] in
                    self?.runSemaphore.wait()
                    d.timerImplement()
                    self?.runSemaphore.signal()
                }
            }
        }
    }
    
    /// 添加监听对象
    @objc public func addObserver(_ observer: GZCTimerHelperDelegate) {
        if !observers.contains(observer) {
            observers.add(observer)
        }
        if _timer == nil {
            timer.resume()
        }
    }
    
    /// 移除监听对象
    @objc public func removeObserver(_ observer: GZCTimerHelperDelegate) {
        if observers.contains(observer) {
            observers.remove(observer)
        }
    }
}
