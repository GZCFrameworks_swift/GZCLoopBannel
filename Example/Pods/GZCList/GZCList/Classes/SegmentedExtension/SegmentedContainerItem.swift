//
//  SegmentedContainerItem.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2021/4/23.
//

// MARK:- SegmentedContainerCollectionCell
/// 分段栏的cell
open class SegmentedContainerCollectionCell: CollectionCellOf<Bool> {
    
    let collectionView: SegmentedContainterCollectionView = SegmentedContainterCollectionView()
    
    let section = CollectionSection()
    
    open override func setup() {
        super.setup()
        collectionView.column = 1
        collectionView.itemSpace = 0
        collectionView.lineSpace = 0
        collectionView.scrollDirection = .horizontal
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        contentView.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        collectionView.form +++ section
    }
    
    open override func didMoveToSuperview() {
        super.didMoveToSuperview()
        collectionView.handler.reloadCollection()
    }
}

// MARK:- SegmentedBarRow
/// 分段栏的Row
public final class SegmentedContainerItem: CollectionItemOf<SegmentedContainerCollectionCell> {
    /// 复用可能导致vc串位，因此不复用
    public override var identifier: String {
        if let outside = outsideIdentifier {
            return outside
        }
        guard let index = indexPath else {
            return "SegmentedContainerItem"
        }
        return "SegmentedContainerItem_\(index.section)_\(index.row)"
    }
    /// 外部传入的identifier
    var outsideIdentifier: String?
    
    /// 内容数组
    public var pageItems: [GZCPageContentType]? {
        didSet {
            items.removeAll()
            for type in pageItems ?? [] {
                items.append(ContainterPageItem { row in
                    row.value = type
                })
            }
            updateCell()
        }
    }
    
    /// 内容Item
    var items: [ContainterPageItem] = []
    
    /// 关联的BarItem
    public weak var relationBarItem: SegmentedBarItem?{
        didSet {
            guard let cell = cell else {
                return
            }
            relationBarItem?.listContainer = cell.collectionView
        }
    }
    /// 关联的BarView
    public var relationBarView: JXSegmentedView? {
        didSet {
            guard let cell = cell else {
                return
            }
            relationBarView?.listContainer = cell.collectionView
        }
    }
    
    /// 是否响应滚动手势
    public var isScrollEnabled: Bool = true
    
    public override func customUpdateCell() {
        super.customUpdateCell()
        guard let cell = cell else {
            return
        }
        cell.section >>> items
        cell.collectionView.isScrollEnabled = false
        cell.collectionView.backgroundColor = backgroundColor
        relationBarItem?.listContainer = cell.collectionView
        relationBarView?.listContainer = cell.collectionView
    }
    
    
    var itemSize: CGFloat = 0
    /// 固定高度与source创建
    /// - Parameter heightOrWidth: 高度或宽度
    /// - Parameter source: JXSegmentedTitleDataSource，用于指定分段内容
    public init(tag: String? = nil, heightOrWidth: CGFloat, pageItems: [GZCPageContentType], identifier: String? = nil, _ initializer: (SegmentedContainerItem) -> Void = { _ in }) {
        super.init(title: nil, tag: tag)
        self.itemSize = heightOrWidth
        self.outsideIdentifier = identifier
        for type in pageItems {
            self.items.append(ContainterPageItem { row in
                row.value = type
            })
        }
        initializer(self)
    }
    
    required init(title: String? = nil, tag: String? = nil) {
        super.init(title: title, tag: tag)
    }
    
    public override func cellWidth(for height: CGFloat) -> CGFloat {
        return itemSize
    }
    
    public override func cellHeight(for width: CGFloat) -> CGFloat {
        return itemSize
    }
}
