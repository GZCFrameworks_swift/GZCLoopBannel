//
//  ResourceHelper.swift
//  GZCLoopBannel_Example
//
//  Created by Guo ZhongCheng on 2020/10/29.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import GZCList
import GZCLoopBannel

/// 获取随机pageControl样式
func getRandomPageControlStyle() -> GZCPageControlStyle {
    let result = arc4random() % 8
    if result == 1 {
        return .system(tintColor: .gray, currentColor: .systemBlue)
    }
    if result == 2 {
        return .fill(tintColor: .systemBlue)
    }
    if result == 3 {
        return .image(activeImage: UIImage(named: "selected")!, unActiveImage: UIImage(named: "unselected")!, dotSpace: 5)
    }
    if result == 4 {
        return .pill(size: CGSize(width: 10, height: 3), indicatorPadding: 5, activeColor: .systemBlue, unActiveColor: .gray)
    }
    if result == 5 {
        return .snake(activeColor: .systemBlue, unActiveColor: .gray, indicatorPadding: 5, indicatorRadius: 5)
    }
    if result == 6 {
        return .number(
            dotFont: .systemFont(ofSize: 13),
            dotTextColor: .white,
            dotUnActiveColor: .gray,
            dotActiveBgColor: .systemBlue,
            dotUnActiveSize: CGSize(width: 10, height: 3),
            dotActiveSize: CGSize(width: 15, height: 20),
            dotSpace: 5
        )
    }
    return .none
}

/// 获取 100~200的随机高度
func getRandomHeight() -> CGFloat {
    return CGFloat(arc4random() % 100) + 100.0
}

/// 获取圆角图片展示Item
func getRoundImageRow() -> ImageItem {
    return ImageItem(url: getRandomImage()) { row in
        row.autoSize = false
        row.corners = CornerType.all(10)
    }
}

/// 获取直角图片展示Item
func getImageRow() -> ImageItem {
    return ImageItem(url: getRandomImage()) { row in
        row.autoSize = false
    }
}

/// 获取随机图片
func getRandomImage() -> String {
    let index:Int = Int(arc4random() % UInt32(images.count))
    return images[index]
}

/// 大图片
let images = [
    "http://pic1.win4000.com/wallpaper/7/53a151ef7c3fe.jpg",
    "http://pic1.win4000.com/mobile/2020-09-24/5f6c35a142966.jpg",
    "http://pic1.win4000.com/wallpaper/7/53a151fd21eef.jpg",
    "http://pic1.win4000.com/mobile/2020-09-24/5f6c35a210bd6.jpg",
    "http://pic1.win4000.com/wallpaper/1/53a15a17d8121.jpg",
    "http://pic1.win4000.com/mobile/2020-09-24/5f6c35a3265d0.jpg",
    "http://pic1.win4000.com/wallpaper/1/53a15a205fe67.jpg",
    "http://pic1.win4000.com/mobile/2020-09-24/5f6c35a43c3ec.jpg",
    "http://pic1.win4000.com/wallpaper/7/53a151e019601.jpg",
    "http://pic1.win4000.com/mobile/2020-09-24/5f6c35a55d4df.jpg",
    "http://pic1.win4000.com/mobile/2020-09-24/5f6c35a65dc48.jpg",
    "http://pic1.win4000.com/mobile/2020-09-24/5f6c35a759a00.jpg",
    "http://pic1.win4000.com/wallpaper/7/53a152032ca56.jpg",
    "http://pic1.win4000.com/wallpaper/7/53a1520b2ce94.jpg",
    "http://pic1.win4000.com/wallpaper/7/53a152109624b.jpg",
    "http://pic1.win4000.com/wallpaper/7/53a152155d7d2.jpg",
    "http://pic1.win4000.com/wallpaper/0/53a13ef661239.jpg",
    "http://pic1.win4000.com/wallpaper/0/53a13ef973775.jpg",
    "http://pic1.win4000.com/wallpaper/0/53a13efcb234b.jpg",
    "http://pic1.win4000.com/wallpaper/0/53a13f002e9f0.jpg",
    "http://pic1.win4000.com/wallpaper/0/53a13f02acd1f.jpg",
    "http://pic1.win4000.com/wallpaper/0/53a13f056b296.jpg",
    "http://pic1.win4000.com/wallpaper/0/53a13f08825ad.jpg",
    "http://pic1.win4000.com/wallpaper/0/53a13f0bd8c85.jpg",
    "http://pic1.win4000.com/mobile/2020-09-24/5f6c598d4d989.jpg",
    "http://pic1.win4000.com/mobile/2020-09-24/5f6c5a4125b29.jpg",
    "http://pic1.win4000.com/mobile/2020-09-24/5f6c5a4444130.jpg",
    "http://pic1.win4000.com/mobile/2020-09-24/5f6c5a45c5ebc.jpg",
    "http://pic1.win4000.com/mobile/2020-09-24/5f6c5a473e96f.jpg",
    "http://pic1.win4000.com/mobile/2020-09-24/5f6c5a487e80e.jpg",
    "http://pic1.win4000.com/mobile/2020-09-24/5f6c5a49e463e.jpg",
    "http://pic1.win4000.com/mobile/2020-09-14/5f5f385765bd8.jpg",
    "http://pic1.win4000.com/mobile/2020-09-14/5f5f38592cc9c.jpg",
    "http://pic1.win4000.com/mobile/2020-09-14/5f5f385a38caf.jpg",
    "http://pic1.win4000.com/mobile/2020-09-14/5f5f385b3518e.jpg",
    "http://pic1.win4000.com/mobile/2020-09-14/5f5f385c3b361.jpg",
    "http://pic1.win4000.com/mobile/2020-09-14/5f5f385d435d2.jpg",
    "https://img.3dmgame.com/uploads/images2/news/20200920/1600588107_559097.jpg",
    "https://img.3dmgame.com/uploads/images2/news/20200920/1600588110_877890.jpg",
    "https://img.3dmgame.com/uploads/images2/news/20200920/1600588111_966387.jpg"
]

extension String {
    func cgFloat(locale: Locale = .current) -> CGFloat {
        let formatter = NumberFormatter()
        formatter.locale = locale
        formatter.allowsFloats = true
        return formatter.number(from: self) as? CGFloat ?? 0
    }
    
    func double(locale: Locale = .current) -> Double? {
        let formatter = NumberFormatter()
        formatter.locale = locale
        formatter.allowsFloats = true
        return formatter.number(from: self)?.doubleValue
    }
}
