//
//  LoopBannelConfig.swift
//  GZCLoopBannel
//
//  Created by Guo ZhongCheng on 2020/10/30.
//

import Foundation

// MARK:- 轮播图配置
public class LoopBannelConfig: NSObject {
    /// 轮播方向
    public var scrollDirection: UICollectionView.ScrollDirection = .horizontal
    /// 轮播边距（默认为0）
    public var bannelInsets: UIEdgeInsets = .zero
    /// 元素宽度 (竖直滚动时为高度)
    public var itemWidth: CGFloat = 300
    /// 元素间距（默认10）
    public var itemSpace: CGFloat = 10
    /// 元素最小缩小的值 ( 范围： 0 ~ 1,  超出范围按最大/小值计算)
    public var minScale: CGFloat = 1.0
    /// 缩小到最小时在垂直轴（水平滚动为y轴，竖直滚动为x轴）的偏移量
    public var minOffset: CGFloat = 0
    /// 中点位置的偏移量
    public var centerOffset: CGFloat = 0
    /// 无限循环- 默认true
    public var infiniteLoop: Bool = true
    /// 自动轮播- 默认true
    public var autoScroll: Bool = true
    /// 滚动间隔时间,默认4秒
    public var autoScrollTimeInterval: Double = 4.0
}

// MARK:- pageControl配置
public class PageControlConfig: NSObject {
    /// 样式
    public var pageControlStyle: GZCPageControlStyle = .none
    /// 位置
    public var pageControlPosition: GZCPageControlPosition = .bottomCenter
    /// pageControl间距（默认为底部5）
    public var pageControlInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 0)
}
