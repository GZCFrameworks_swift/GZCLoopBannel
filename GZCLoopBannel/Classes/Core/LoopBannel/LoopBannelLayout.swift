//
//  LoopBannelLayout.swift
//  GZCLoopBannel
//
//  Created by Guo ZhongCheng on 2020/10/26.
//

class LoopBannelLayout: UICollectionViewFlowLayout {
    
    /// 最大放大倍数
//    var maxScaleValue: CGFloat = 1.05
    /// 最小缩小倍数
    var minScaleValue: CGFloat = 0.95
    /// 缩小后的偏移量
    var minOffset: CGFloat = 0.0
    /// 中点位置的偏移量
    var centerOffset: CGFloat = 0
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        if minScaleValue == 1 {
            return super.layoutAttributesForElements(in: rect)
        }
        
        // 深拷贝数组
        var array = [UICollectionViewLayoutAttributes]()
        for attr in super.layoutAttributesForElements(in: rect) ?? [] {
            array += (attr.copy() as! UICollectionViewLayoutAttributes)
        }
        
        var visiableRect = CGRect.zero
        visiableRect.origin = self.collectionView!.contentOffset
        visiableRect.size = self.collectionView!.bounds.size
        
        if scrollDirection == .horizontal {
            for attributes in array {
                if attributes.frame.intersects(rect) {
                    let distance = visiableRect.midX - attributes.center.x + centerOffset
                    let absDistance = abs(distance)
                    if absDistance < visiableRect.width + attributes.frame.width {
                        let zoom = max(minScaleValue, (1 - minScaleValue) * ( 1 - absDistance / ((attributes.frame.width * 2 + minimumLineSpacing) * 0.5)) + minScaleValue)
                        let zOffScale = (1 - zoom) / (1 - minScaleValue)
                        let offsetX = (attributes.frame.width - minimumLineSpacing * 0.5) * (1 - zoom) * (distance < 0 ? -1 : 1) * 0.5
                        attributes.transform3D = CATransform3DMakeScale(zoom, zoom, 1.0)
                        attributes.transform3D = CATransform3DTranslate(attributes.transform3D, offsetX, minOffset * zOffScale, 0)
                    }
                }
            }
            return array
        } else {
            for attributes in array {
                if attributes.frame.intersects(rect) {
                    let distance = visiableRect.midY - attributes.center.y + centerOffset
                    let absDistance = abs(distance)
                    if absDistance < visiableRect.height + attributes.frame.height {
                        let zoom = max(minScaleValue, (1 - minScaleValue) * ( 1 - absDistance / ((attributes.frame.width * 2 + minimumLineSpacing) * 0.5)) + minScaleValue)
                        let zOffScale = (1 - zoom) / (1 - minScaleValue)
                        let offsetY = (attributes.frame.height - minimumLineSpacing * 0.5) * (1 - zoom) * (distance < 0 ? -1 : 1) * 0.5
                        attributes.transform3D = CATransform3DMakeScale(zoom, zoom, 1.0)
                        attributes.transform3D = CATransform3DTranslate(attributes.transform3D, minOffset * zOffScale, offsetY, 0)
                    }
                }
            }
            return array
        }
    }
    
    /// 计算滚动停止位置
    func pointTargetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        var offsetAdjustment: CGFloat = CGFloat(MAXFLOAT)
        if scrollDirection == .horizontal {
            let horizontalCenter_X: CGFloat = proposedContentOffset.x + self.collectionView!.bounds.width * 0.5 + centerOffset
            let targetRect: CGRect = CGRect(x: proposedContentOffset.x, y: 0, width: self.collectionView!.bounds.size.width, height: self.collectionView!.bounds.size.height)
            let array = super.layoutAttributesForElements(in: targetRect)
            for attributes in array ?? [] {
                let itemHorizontalCenter_X = attributes.center.x
                if abs(itemHorizontalCenter_X - horizontalCenter_X) < abs(offsetAdjustment) {
                    offsetAdjustment = itemHorizontalCenter_X - horizontalCenter_X
                }
            }
            return CGPoint(x: proposedContentOffset.x + offsetAdjustment, y: proposedContentOffset.y)
        } else {
            let horizontalCenter_Y: CGFloat = proposedContentOffset.y + self.collectionView!.bounds.height * 0.5 + centerOffset
            let targetRect: CGRect = CGRect(x: 0, y: proposedContentOffset.y, width: self.collectionView!.bounds.size.width, height: self.collectionView!.bounds.size.height)
            let array = super.layoutAttributesForElements(in: targetRect)
            for attributes in array ?? [] {
                let itemHorizontalCenter_Y = attributes.center.y
                if abs(itemHorizontalCenter_Y - horizontalCenter_Y) < abs(offsetAdjustment) {
                    offsetAdjustment = itemHorizontalCenter_Y - horizontalCenter_Y
                }
            }
            return CGPoint(x: proposedContentOffset.x, y: proposedContentOffset.y + offsetAdjustment)
        }
        
    }
    
    /// 获取选中item时Collectionview的偏移量
    func targetPositionToSelectIndexPath(at indexPath: IndexPath) -> CGPoint {
        guard let frame = self.layoutAttributesForItem(at: indexPath)?.frame,
              let collection = collectionView else {
            return .zero
        }
        if scrollDirection == .horizontal {
            let targetX = frame.midX - collection.bounds.width * 0.5 - centerOffset
            return CGPoint(x: targetX, y: collection.contentOffset.y)
        } else {
            let targetY = frame.midY - collection.bounds.height * 0.5 - centerOffset
            return CGPoint(x: collection.contentOffset.x, y: targetY)
        }
    }
    
    //当collectionView的显示范围发生改变的时候，是否重新布局
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        if minScaleValue == 1.0 {
            return false
        }
        return true
    }
    
    override var collectionViewContentSize: CGSize {
        var size = super.collectionViewContentSize
        guard let collectionView = collectionView else {
            return size
        }
        let section = collectionView.numberOfSections
        guard
            let attr = super.layoutAttributesForItem(at: IndexPath(row: collectionView.numberOfItems(inSection: section - 1) - 1, section: section - 1))
        else {
            return size
        }
        if scrollDirection == .horizontal {
            let itemWidth = attr.frame.width
            let collectionWidth = collectionView.bounds.size.width
            let offset = (collectionWidth - itemWidth) * 0.5
            size.width += offset
        } else {
            let itemHeight = attr.frame.height
            let collectionHeight = collectionView.bounds.size.height
            let offset = (collectionHeight - itemHeight) * 0.5
            size.height += offset
        }
        return size
    }
    
}
