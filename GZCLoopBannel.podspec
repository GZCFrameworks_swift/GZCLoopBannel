#
# Be sure to run `pod lib lint GZCLoopBannel.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'GZCLoopBannel'
  s.version          = '0.1.2'
  s.summary          = '轮播图控件'

  s.description      = <<-DESC
  轮播图控件，基于GZCList，同时提供GZCList可快速使用的Row和Item
                       DESC

  s.homepage         = 'https://gitlab.com/GZCFrameworks_swift/GZCLoopBannel'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Guo ZhongCheng' => 'gzhongcheng@qq.com' }
  s.source           = { :git => 'https://gitlab.com/GZCFrameworks_swift/GZCLoopBannel.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'
  s.swift_version = "5.1"
  
  s.source_files = 'GZCLoopBannel/Classes/**/*'
  s.dependency 'GZCList'
  s.dependency 'GZCTimerHelper'

end
