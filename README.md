# GZCLoopBannel

轮播图控件，基于[GZCList](https://gitlab.com/GZCFrameworks_swift/GZCList)，同时提供GZCList可快速使用的带bannel的Row和Item以及用作自定义header的方法(可参考demo)

### 如何安装

GZCLoopBannel 支持 [CocoaPods](https://cocoapods.org). 安装 添加下面代码到项目的 Podfile 文件中:

```
pod 'GZCLoopBannel'
```

### 使用方法

#### 1、快速创建

```
let bannelView = GZCLoopBannelView()
var items = [ImageItem]() // Item可以为任何的CollectionItem
bannelView.items = items
```

### 2、属性配置

`GZCLoopBannelView`通过`bannelConfig`属性来设置轮播内容的样式，通过`pageControlConfig`属性来设置`PageControl`的样式:

`LoopBannelConfig`包含下列属性设置：

> **scrollDirection**: 轮播方向
> **bannelInsets**: 轮播间距（默认为全0）
> **itemWidth**: 元素宽度 (竖直滚动时为高度)
> **itemSpace**: 元素间距（默认10）
> **scaleIncrement**: 元素缩放的增量 ( 放大与缩小的增量 , 范围： 0 ~ 0.99,  超出范围按最大/小值计算)
> **minOffset**: 缩小到最小时在垂直轴（水平滚动为y轴，竖直滚动为x轴）的偏移量
> **centerOffset**: 中点位置的偏移量
> **infiniteLoop**: 无限循环- 默认true
> **autoScroll**: 自动轮播- 默认true
> **autoScrollTimeInterval**: 滚动间隔时间,默认4秒

`PageControlConfig`包含下列属性设置：

>**pageControlStyle**：样式，默认无pageControl
>
>**pageControlPosition**：位置，默认为底部居中
>
>**pageControlInsets**：四边间距（默认为底部5）

### 3、PageControl已有样式预览

| 样式          | 效果                                                  |
| ------------- | ----------------------------------------------------- |
| .system       | ![image-20201030165829581](./Images/system.png)       |
| .fill         | ![image-20201030170904823](./Images/fill.gif)         |
| .pill         | ![image-20201030170904823](./Images/pill.gif)         |
| .longActivite | ![image-20201030170904823](./Images/longActivite.gif) |
| .snake        | ![image-20201030170904823](./Images/snake.gif)        |
| .image        | ![image-20201030170904823](./Images/image.png)        |
| .number       | ![image-20201030170904823](./Images/number.gif)       |

### 4、Row与Item的使用

Row和Item的创建与View大同小异，添加方法与GZCList的其他Row和Item相同，如：

```
section <<< GZCLoopBannelRow() { row in
    var items = [ImageItem]()
    for _ in 0 ..< 10 {
        items += getRoundImageRow()
    }
    row.value = items
    row.cellHeight = getRandomHeight()
    row.pageControlConfig.pageControlStyle = getRandomPageControlStyle()
    row.bannelConfig.itemWidth = CGFloat(arc4random() % 50 + 300)
}
```

由于使用了复用机制，因此每次row出现时都会在第一个轮播内容的位置

## 其他

demo中提供了具体属性设置的实例程序，以及已写好的样式可供参考：

|轮播内容|pageControl|一些样式|
| ---------------------------- | -------------------------- | ---------------------- |
| ![](./Images/bannelDemo.png) | ![](./Images/pageDemo.png) | ![](./Images/demo.png) |

## Author

Guo ZhongCheng, gzhongcheng@qq.com

## License

GZCLoopBannel is available under the MIT license. See the LICENSE file for more info.
