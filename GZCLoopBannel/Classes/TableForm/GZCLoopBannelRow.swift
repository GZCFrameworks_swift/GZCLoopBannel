//
//  GZCLoopBannelRow.swift
//  GZCLoopBannel
//
//  Created by Guo ZhongCheng on 2020/10/29.
//

import GZCList

open class GZCLoopBannelCell: TableCellOf<[CollectionItem]> {
    let bannelView = GZCLoopBannelView()
    
    open override func setup() {
        super.setup()
        selectionStyle = .none
        
        contentView.addSubview(bannelView)
        
        bannelView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}

open class _GZCLoopBannelRow: TableRowOf<GZCLoopBannelCell> {
    
    // 轮播图配置
    public var bannelConfig: LoopBannelConfig = LoopBannelConfig()
    // pageControl配置
    public var pageControlConfig: PageControlConfig = PageControlConfig()
    
    open override func customUpdateCell() {
        super.customUpdateCell()
        guard let cell = cell else {
            return
        }
        
        cell.contentView.snp.updateConstraints { (make) in
            make.edges.equalTo(contentInsets)
        }
        
        cell.bannelView.bannelConfig = bannelConfig
        cell.bannelView.pageControlConfig = pageControlConfig
        cell.bannelView.items = value ?? []
    }
    
    open override var identifier: String? {
        return "_GZCLoopBannelRow\(Int(bannelConfig.itemWidth))"
    }
    
    open override func didEndDisplay() {
        super.didEndDisplay()
        guard let cell = cell else {
            return
        }
        cell.bannelView.stopLoop()
    }
    
    open override func willDisplay() {
        super.willDisplay()
        guard let cell = cell else {
            return
        }
        cell.bannelView.beginLoop()
    }
    
    public required init(title: String? = nil, tag: String? = nil) {
        super.init(title: title, tag: tag)
        contentInsets = .zero
    }
}

final public class GZCLoopBannelRow:_GZCLoopBannelRow, RowType {
}
