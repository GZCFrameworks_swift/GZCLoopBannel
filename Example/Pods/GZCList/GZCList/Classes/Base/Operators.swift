//
//  Operators.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2020/6/2.
//

// MARK:- 自定义运算符
/// 定义优先级组
precedencegroup FormPrecedence {
    associativity: left                      // 结合方向:left, right or none
    higherThan: LogicalConjunctionPrecedence // 优先级,比&&运算高
//    assignment: false                   // true=赋值运算符,false=非赋值运算符
}

precedencegroup  SectionPrecedence {
    associativity: left             // 结合方向:left
    higherThan: FormPrecedence      // 优先级,比Form高
}

// MARK:- 声明操作符
// 增加
/// +++  返回结果为 Form
infix operator +++ : FormPrecedence
/// +++! 返回结果为 Form，并通知代理
infix operator +++! : FormPrecedence
/// <<< 返回结果为 Section
infix operator <<< :  SectionPrecedence
/// <<< 返回结果为 Section，并通知代理
infix operator <<<! :  SectionPrecedence

// 替换
/// >>> 替换 元素 并通知代理
infix operator >>> : FormPrecedence

// 移除
/// ---- 移除所有元素并通知代理
postfix operator ---

// MARK:- ---
/**
 移除Form的所有元素, 并通知到代理

- parameter right: Form

- returns: Form
*/
@discardableResult
public postfix func ---(right: BaseForm) -> BaseForm {
    if (right.count == 0) {
        return right
    }
    let sections = right.allSections
    right.removeAll()
    if let delegate = right.delegate {
        delegate.sectionsHaveBeenRemoved(sections, at: IndexSet(integersIn: 0 ..< sections.count))
    }
    return right
}
/**
 移除Section的所有元素, 并通知到代理

- parameter right: Form

- returns: Form
*/
@discardableResult
public postfix func ---(right: BaseSection) -> BaseSection {
    if (right.count == 0) {
        return right
    }
    let rows = right.allRows
    right.removeAll()
    if
        let form = right.form,
        let delegate = form.delegate
    {
        guard let section = form.allSections.firstIndex(of: right) else { return right }
        var indexPaths = [IndexPath]()
        for i in 0 ..< rows.count {
            indexPaths.append(IndexPath(row: i, section: section))
        }
        delegate.rowsHaveBeenRemoved(rows, at: indexPaths)
    }
    return right
}

// MARK:- +=
/**
 添加 元素 到 数组
 
 - parameter lhs: 数组
 - parameter rhs: 新的元素
 */
public func += <C: Any>(lhs: inout Array<C>, rhs: C?) {
    guard let r = rhs else {
        return
    }
    lhs.append(r)
}

/**
 添加 Row 的集合到 Section
 
 - parameter lhs: section
 - parameter rhs: rows 的集合
 */
public func += <C: Collection>(lhs: inout BaseSection, rhs: C) where C.Iterator.Element == BaseRow {
    lhs.append(contentsOf: rhs)
}

/**
 添加 Section 的集合到 Form
 
 - parameter lhs: form
 - parameter rhs: sections 的集合
 */
public func += <C: Collection>(lhs: inout BaseForm, rhs: C) where C.Iterator.Element == BaseSection {
    lhs.append(contentsOf: rhs)
}

// 运算符帮助泛型类
public class ListOperatorsHelper<Form: BaseForm, Section: BaseSection, Row: BaseRow>: NSObject {
    // MARK:- 添加section
    /**
     添加 Section 到 Form
     */
    @discardableResult
    public static func add (_ section: Section, to form: Form) -> Form {
        form.append(section)
        return form
    }

    // MARK:- 添加section并更新
    /**
     添加 Section 到 Form, 并通知到代理
     */
    @discardableResult
    public static func addAndUpdate(_ section: Section, to form: Form) -> Form {
        form.append(section)
        if let delegate = form.delegate {
            delegate.sectionsHaveBeenAdded([section], at: IndexSet(integersIn: form.allSections.count - 1 ..< form.allSections.count))
        }
        return form
    }

    // MARK:- 添加row
    /**
     添加Row到Section
     */
    @discardableResult
    public static func add(_ row: Row, to section: Section) -> Section {
        section.append(row)
        return section
    }

    // MARK:- 添加row并更新
    /**
     添加Row到Section, 并通知到代理
     */
    @discardableResult
    public static func addAndUpdate(_ row: Row, to section: Section) -> Section {
        section.append(row)
        if
            let form = section.form,
            let delegate = form.delegate
        {
            guard let sectionIndex = form.allSections.firstIndex(of: section) else {
                return section
            }
            delegate.rowsHaveBeenAdded([row], at: [IndexPath(row: section.allRows.count - 1, section: sectionIndex)])
        }
        return section
    }
    /**
     添加Row数组到Section, 并通知到代理
     */
    @discardableResult
    public static func addAndUpdate(_ rows: [Row], to section: Section) -> Section {
        
        if
            let form = section.form,
            let delegate = form.delegate
        {
            guard let sectionIndex = form.allSections.firstIndex(of: section) else {
                section.append(contentsOf: rows)
                return section
            }
            var index = section.allRows.count
            var indexPaths = [IndexPath]()
            for row in rows {
                section.append(row)
                indexPaths.append(IndexPath(row: index, section: sectionIndex))
                index += 1
            }
            delegate.rowsHaveBeenAdded(rows, at: indexPaths)
        } else {
            section.append(contentsOf: rows)
        }
        return section
    }

    // MARK:- 替换
    /**
     替换Form的所有Section, 并通知到代理
    - parameter newSections: 要替换的Section数组
    */
    @discardableResult
    public static func replace(_ newSections: [Section], to form: Form) -> Form {
        let oldSections = form.allSections
        form.replaceSubrange(0 ..< oldSections.count, with: newSections)
        if let delegate = form.delegate {
            delegate.sectionsHaveBeenReplaced(oldSections: oldSections, newSections: newSections, at: IndexSet(integersIn: 0 ..< oldSections.count))
        }
        return form
    }
    
    /**
     替换Section数组到指定范围, 并通知到代理
    - parameter rangeSection: 元组，( 范围 ，要替换的Section数组 )
    */
    @discardableResult
    public static func replace(_ rangeSection: (Range<Int>, [Section]), to form: Form) -> Form {
        var oldSections = [Section]()
        for i in min(rangeSection.0.lowerBound, form.allSections.count - 1) ..< min(rangeSection.0.upperBound, form.allSections.count) {
            oldSections.append(form[i] as! Section)
        }
        form.replaceSubrange(rangeSection.0, with: rangeSection.1)
        if let delegate = form.delegate {
            delegate.sectionsHaveBeenReplaced(oldSections: oldSections, newSections: rangeSection.1, at: IndexSet(integersIn: 0 ..< oldSections.count))
        }
        return form
    }
    
    /**
     替换Section的所有Row，并通知到代理
    - parameter newRows: 要替换的Row数组
    */
    @discardableResult
    public static func replace(_ newRows: [Row], to section: Section, useAnimation: Bool = true) -> Section {
        let oldRows = section.allRows
        var replaceIndexPaths = [IndexPath]()
        for i in 0 ..< oldRows.count {
            let row = oldRows[i] as! Row
            if let indexPath = row.indexPath {
                replaceIndexPaths.append(indexPath)
            }
        }
        section.replaceSubrange(0 ..< oldRows.count, with: newRows)
        if
            let form = section.form,
            let delegate = form.delegate
        {
            if useAnimation {
                delegate.rowsHaveBeenReplaced(oldRows: oldRows, newRows: newRows, at: replaceIndexPaths)
            } else {
                UIView.performWithoutAnimation {
                    delegate.rowsHaveBeenReplaced(oldRows: oldRows, newRows: newRows, at: replaceIndexPaths)
                }
            }
        }
        return section
    }
    
    /**
     替换Row数组到指定范围, 并通知到代理
    - parameter ramgeRows: 元组，( 范围 ，要替换的Row数组 )
    */
    @discardableResult
    public static func replace(_ ramgeRows: (Range<Int>, [Row]), to section: Section, useAnimation: Bool = true) -> Section {
        var oldRows = [Row]()
        let newRows = ramgeRows.1
        var replaceIndexPaths = [IndexPath]()
        for i in min(ramgeRows.0.lowerBound, section.allRows.count - 1) ..< min(ramgeRows.0.upperBound, section.allRows.count) {
            let row = section[i] as! Row
            oldRows.append(row)
            if let indexPath = row.indexPath {
                replaceIndexPaths.append(indexPath)
            }
        }
        section.replaceSubrange(ramgeRows.0, with: ramgeRows.1)
        if
            let form = section.form,
            let delegate = form.delegate
        {
            if useAnimation {
                delegate.rowsHaveBeenReplaced(oldRows: oldRows, newRows: newRows, at: replaceIndexPaths)
            } else {
                UIView.performWithoutAnimation {
                    delegate.rowsHaveBeenReplaced(oldRows: oldRows, newRows: newRows, at: replaceIndexPaths)
                }
            }
        }
        return section
    }
}
