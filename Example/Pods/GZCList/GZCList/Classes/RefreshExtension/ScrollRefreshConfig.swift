//
//  ScrollRefreshConfig.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2020/10/21.
//

import ESPullToRefresh

public typealias RefreshAnimatorCreater = (() -> (ESRefreshProtocol & ESRefreshAnimatorProtocol))
public class ScrollRefreshConfig {
    public static let shared = ScrollRefreshConfig()
    
    /// 统一生成默认下拉刷新header方法（使用`addHeaderRefresher()`方法添加下拉刷新时将使用此方法获取刷新header）
    public var headerAnimator: RefreshAnimatorCreater?
    /// 统一生成默认加载更多footer方法（使用`addFooterRefresher()`方法添加下拉刷新时将使用此方法获取刷新footer）
    public var footerAnimator: RefreshAnimatorCreater?
}
