//
//  SegmentedBarCollectionHeader.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2021/4/25.
//

import JXSegmentedView

public func SegmentedCollectionHeader(height: CGFloat, dataSource: JXSegmentedBaseDataSource, backgroundColor: UIColor? = .white, _ initializer:  ((_ segmentedView: JXSegmentedView) -> Void)? = nil) -> CollectionHeaderFooterViewRepresentable {
    // 自定义header
    var headerProvider = CollectionHeaderFooterView<SegmentedBarCollectionHeader>.init { (view) in
        view.segmentView.dataSource = dataSource
        view.backgroundColor = backgroundColor
        initializer?(view.segmentView)
    }
    headerProvider.height = { height }
    return headerProvider
}

public class SegmentedBarCollectionHeader: UICollectionReusableView {
    public let segmentView: JXSegmentedView = JXSegmentedView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupSubviews()
    }
    
    func setupSubviews() {
        addSubview(segmentView)
        segmentView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}
