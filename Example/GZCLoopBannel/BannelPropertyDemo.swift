//
//  BannelPropertyDemo.swift
//  GZCLoopBannel_Example
//
//  Created by Guo ZhongCheng on 2020/10/30.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import GZCList
import GZCLoopBannel
import Kingfisher

class BannelPropertyDemo: FormTableViewController {
    
    let section = TableSection()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.refresh, target: self, action: #selector(updateBannel))
        
        // 自定义header
        var headerProvider = TableHeaderFooterView<GZCLoopBannelView>(.callback { () -> GZCLoopBannelView in
            let bannelView = GZCLoopBannelView()
            var items = [ImageItem]()
            for _ in 0 ..< 10 {
                items += getRoundImageRow()
            }
            bannelView.items = items
            bannelView.backgroundColor = .white
            bannelView.bannelInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
            return bannelView
        })
        headerProvider.height = { 160 }
        section.header = headerProvider
        
        form +++ section
            <<< LabelRow(){ row in
                row.value = "调整下面的属性后，点击右上角的刷新按钮查看效果（可能会出现位置暂时偏移的情况，滚动后即可正常展示）"
                row.contentInsets = .zero
            }
            <<< SwitchRow("无限轮播", tag: "infiniteLoop") { row in
                row.value = true
            }
            <<< SwitchRow("自动轮播", tag: "autoScroll") { row in
                row.value = true
            }
            <<< TextFieldRow("自动轮播时间间隔", tag: "autoScrollTimeInterval") { row in
                row.placeHolder = "单位：秒，默认4.0"
                row.keyboardType = .numbersAndPunctuation
            }
            <<< TextFieldRow("缩放倍数", tag: "minScale") { row in
                row.placeHolder = "范围限制为0~0.99，默认0.05"
                row.keyboardType = .numbersAndPunctuation
            }
            <<< TextFieldRow("内容宽度", tag: "itemWidth") { row in
                row.placeHolder = "不小于零,默认300"
                row.keyboardType = .numbersAndPunctuation
            }
            <<< TextFieldRow("内容间距", tag: "itemSpace") { row in
                row.placeHolder = "建议不小于零,默认10"
                row.keyboardType = .numbersAndPunctuation
            }
            <<< TextFieldRow("缩小位移", tag: "minOffset") { row in
                row.placeHolder = "位移垂直于滚动轴，默认0"
                row.keyboardType = .numbersAndPunctuation
            }
            <<< TextFieldRow("展示中心点位移", tag: "centerOffset") { row in
                row.placeHolder = "默认0"
                row.keyboardType = .numbersAndPunctuation
            }
    }
    
    @objc func updateBannel() {
        let config = LoopBannelConfig()
        if let infiniteLoop = (form.rowBy(tag: "infiniteLoop") as! SwitchRow).value {
            config.infiniteLoop = infiniteLoop
        }
        if let autoScrol = (form.rowBy(tag: "autoScroll") as! SwitchRow).value {
            config.autoScroll = autoScrol
        }
        if let scaleIncrement = (form.rowBy(tag: "minScale") as! TextFieldRow).value {
            config.minScale = scaleIncrement.cgFloat()
        }
        if let itemWidth = (form.rowBy(tag: "itemWidth") as! TextFieldRow).value {
            config.itemWidth = itemWidth.cgFloat()
        }
        if let itemSpace = (form.rowBy(tag: "itemSpace") as! TextFieldRow).value {
            config.itemSpace = itemSpace.cgFloat()
        }
        if let minOffset = (form.rowBy(tag: "minOffset") as! TextFieldRow).value {
            config.minOffset = minOffset.cgFloat()
        }
        if let centerOffset = (form.rowBy(tag: "centerOffset") as! TextFieldRow).value {
            config.centerOffset = centerOffset.cgFloat()
        }
        if let autoScrollTimeInterval = (form.rowBy(tag: "autoScrollTimeInterval") as! TextFieldRow).value {
            config.autoScrollTimeInterval = autoScrollTimeInterval.double() ?? 4
        }
        /// 在这里直接修改confi，可能会导致位置有所偏移（滚动一下就好了）
        headerBannelView()?.bannelConfig = config
    }
    
    func headerBannelView() -> GZCLoopBannelView? {
        return section.headerView as? GZCLoopBannelView
    }
    
    
    deinit {
        /// 此控制器中会创建大量图片在内存中占用，可以使用此方法快速清除内存中的图片
        ImageCache.default.clearMemoryCache()
        print("----释放了————")
    }
}
