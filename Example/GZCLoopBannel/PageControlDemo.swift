//
//  PageControlDemo.swift
//  GZCLoopBannel_Example
//
//  Created by Guo ZhongCheng on 2020/10/30.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import GZCList
import GZCLoopBannel
import Kingfisher

class PageControlDemo: FormTableViewController {
    
    let section = TableSection()
    
    weak var currentPosition: ButtonRow?
    weak var currentStyle: ButtonRow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.refresh, target: self, action: #selector(updateAction))
        
        // 自定义header
        var headerProvider = TableHeaderFooterView<GZCLoopBannelView>(.callback { () -> GZCLoopBannelView in
            let bannelView = GZCLoopBannelView()
            var items = [ImageItem]()
            for _ in 0 ..< 10 {
                items += getRoundImageRow()
            }
            bannelView.items = items
            bannelView.backgroundColor = .white
            bannelView.bannelInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
            return bannelView
        })
        headerProvider.height = { 140 }
        section.header = headerProvider
        
        form +++ section
            <<< LabelRow(){ row in
                row.value = "调整下面的属性后，点击右上角的刷新按钮查看效果（自定义的样式未展示，可使用 .custom 来设置）"
                row.contentInsets = .zero
            }
            <<< LabelRow("样式") { row in
                row.backgroundColor = .gray
                row.cellHeight = 30
            }
            <<< buttonRow("(当前)无", tag: "none")
            <<< buttonRow("系统样式", tag: "system")
            <<< buttonRow("动画填充圆心", tag: "fill")
            <<< buttonRow("横线滑块", tag: "pill")
            <<< buttonRow("长滑块", tag: "longActivite")
            <<< buttonRow("拉伸动画", tag: "snake")
            <<< buttonRow("自定义图片", tag: "image")
            <<< buttonRow("选中时显示数字，其他横线", tag: "number")
            <<< LabelRow("间距设置") { row in
                row.backgroundColor = .gray
                row.cellHeight = 30
            }
            <<< TextFieldRow("上", tag: "top") { row in
                row.keyboardType = .numbersAndPunctuation
                row.placeHolder = "仅在position为TopXXX时生效"
            }
            <<< TextFieldRow("下", tag: "bottom") { row in
                row.keyboardType = .numbersAndPunctuation
                row.placeHolder = "仅在position为BottomXXX时生效"
            }
            <<< TextFieldRow("左", tag: "left") { row in
                row.keyboardType = .numbersAndPunctuation
                row.placeHolder = "不宜超过宽度"
            }
            <<< TextFieldRow("右", tag: "right") { row in
                row.keyboardType = .numbersAndPunctuation
                row.placeHolder = "不宜超过宽度"
            }
            <<< LabelRow("位置") { row in
                row.backgroundColor = .gray
                row.cellHeight = 30
            }
            <<< buttonRow("上左", tag: "topLeft")
            <<< buttonRow("上中", tag: "topCenter")
            <<< buttonRow("上右", tag: "topRight")
            <<< buttonRow("下左", tag: "bottomLeft")
            <<< buttonRow("(当前)下中", tag: "bottomCenter")
            <<< buttonRow("下右", tag: "bottomRight")
        
        currentPosition = form.rowBy(tag: "bottomCenter") as? ButtonRow
        currentStyle = form.rowBy(tag: "none") as? ButtonRow
    }
    
    func buttonRow(_ title: String, tag: String) -> ButtonRow {
        return ButtonRow(title, tag: tag).onCellSelection {[weak self] (c, r) in
            self?.updateBannel(tag)
        }
    }
    
    @objc func updateAction() {
        updateBannel()
    }
    
    func changePositionSelect(_ tag: String) {
        if let currentTitle = currentPosition?.title {
            let newTitle = currentTitle.replacingOccurrences(of: "(当前)", with: "")
            currentPosition?.title = newTitle
            currentPosition?.updateCell()
        }
        let newPosition = form.rowBy(tag: tag) as? ButtonRow
        if let title = newPosition?.title {
            let newTitle = "(当前)" + title
            newPosition?.title = newTitle
            newPosition?.updateCell()
            currentPosition = newPosition
        }
    }
    
    func changeStyleSelect(_ tag: String) {
        if let currentTitle = currentStyle?.title {
            let newTitle = currentTitle.replacingOccurrences(of: "(当前)", with: "")
            currentStyle?.title = newTitle
            currentStyle?.updateCell()
        }
        let newPosition = form.rowBy(tag: tag) as? ButtonRow
        if let title = newPosition?.title {
            let newTitle = "(当前)" + title
            newPosition?.title = newTitle
            newPosition?.updateCell()
            currentStyle = newPosition
        }
    }
    
    func updateBannel(_ tag: String? = nil) {
        let config = PageControlConfig()
        config.pageControlStyle = headerBannelView().pageControlConfig.pageControlStyle
        config.pageControlPosition = headerBannelView().pageControlConfig.pageControlPosition
        if let t = tag {
            if t == "topLeft" {
                config.pageControlPosition = .topLeft
                changePositionSelect(t)
            }
            if t == "topCenter" {
                config.pageControlPosition = .topCenter
                changePositionSelect(t)
            }
            if t == "topRight" {
                config.pageControlPosition = .topRight
                changePositionSelect(t)
            }
            if t == "bottomLeft" {
                config.pageControlPosition = .bottomLeft
                changePositionSelect(t)
            }
            if t == "bottomCenter" {
                config.pageControlPosition = .bottomCenter
                changePositionSelect(t)
            }
            if t == "bottomRight" {
                config.pageControlPosition = .bottomRight
                changePositionSelect(t)
            }
            
            if t == "none" {
                config.pageControlStyle = .none
                changeStyleSelect(t)
            }
            if t == "system" {
                config.pageControlStyle = .system(tintColor: .gray, currentColor: .systemBlue)
                changeStyleSelect(t)
            }
            if t == "fill" {
                config.pageControlStyle = .fill(tintColor: .systemBlue)
                changeStyleSelect(t)
            }
            if t == "image" {
                config.pageControlStyle = .image(activeImage: UIImage(named: "selected")!, unActiveImage: UIImage(named: "unselected")!, dotSpace: 5)
                changeStyleSelect(t)
            }
            if t == "pill" {
                config.pageControlStyle = .pill(size: CGSize(width: 10, height: 3), indicatorPadding: 5, activeColor: .systemBlue, unActiveColor: .gray)
                changeStyleSelect(t)
            }
            if t == "longActivite" {
                config.pageControlStyle = .longActivite()
                changeStyleSelect(t)
            }
            if t == "snake" {
                config.pageControlStyle = .snake(activeColor: .systemBlue, unActiveColor: .gray, indicatorPadding: 5, indicatorRadius: 5)
                changeStyleSelect(t)
            }
            if t == "number" {
                config.pageControlStyle = .number(
                    dotFont: .systemFont(ofSize: 13),
                    dotTextColor: .white,
                    dotUnActiveColor: .gray,
                    dotActiveBgColor: .systemBlue,
                    dotUnActiveSize: CGSize(width: 10, height: 3),
                    dotActiveSize: CGSize(width: 15, height: 20),
                    dotSpace: 5
                )
                changeStyleSelect(t)
            }
        }
        var pageInset = UIEdgeInsets()
        if let top = (form.rowBy(tag: "top") as! TextFieldRow).value {
            pageInset.top = top.cgFloat()
        }
        if let bottom = (form.rowBy(tag: "bottom") as! TextFieldRow).value {
            pageInset.bottom = bottom.cgFloat()
        }
        if let left = (form.rowBy(tag: "left") as! TextFieldRow).value {
            pageInset.left = left.cgFloat()
        }
        if let right = (form.rowBy(tag: "right") as! TextFieldRow).value {
            pageInset.right = right.cgFloat()
        }
        config.pageControlInsets = pageInset
        
        headerBannelView().pageControlConfig = config
    }
    
    func headerBannelView() -> GZCLoopBannelView {
        return section.headerView as! GZCLoopBannelView
    }
    
    
    deinit {
        /// 此控制器中会创建大量图片在内存中占用，可以使用此方法快速清除内存中的图片
        ImageCache.default.clearMemoryCache()
        print("----释放了————")
    }
}

