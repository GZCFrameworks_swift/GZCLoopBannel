//
//  GZCLoopBannelCollectionHeader.swift
//  GZCLoopBannel
//
//  Created by Guo ZhongCheng on 2020/10/29.
//

import Foundation

public class GZCLoopBannelCollectionHeader: UICollectionReusableView {
    
    public let bannelView = GZCLoopBannelView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(bannelView)
        bannelView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
