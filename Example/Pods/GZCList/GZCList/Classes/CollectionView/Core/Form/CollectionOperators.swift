//
//  CollectionOperators.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2020/9/15.
//

// MARK:- +++
/**
 添加 Section 到 Form
 
 - parameter left:  form
 - parameter right: 需要添加的 section
 
 - returns: 添加后的 form
 */
@discardableResult
public func +++ (left: CollectionForm, right: CollectionSection) -> CollectionForm {
    return ListOperatorsHelper.add(right, to: left)
}
/**
 添加 Row 到 Form 的最后一个 Section
 
 - parameter left:  form
 - parameter right: row
 */
@discardableResult
public func +++ (left: CollectionForm, right: CollectionItem) -> CollectionForm {
    if let section = left.allSections.last as? CollectionSection {
        ListOperatorsHelper.add(right, to: section)
    } else {
        let section = CollectionSection()
        ListOperatorsHelper.add(right, to: section)
        ListOperatorsHelper.add(section, to: left)
    }
    return left
}
/**
 用两个Section相加创建Form
 
 - parameter left:  第一个 section
 - parameter right: 第二个 section
 
 - returns: 创建好的Form
 */
@discardableResult
public func +++ (left: CollectionSection, right: CollectionSection) -> CollectionForm {
    let form = CollectionForm()
    ListOperatorsHelper.add(left, to: form)
    ListOperatorsHelper.add(right, to: form)
    return form
}

/**
 用两个Section相加创建CollectionForm, 每个Section中包含一个Row
 
 - parameter left:  第一个Section中的Row
 - parameter right: 第二个Section中的Row
 
 - returns: 创建好的Form
 */
@discardableResult
public func +++ (left: CollectionItem, right: CollectionItem) -> CollectionForm {
    let section1 = CollectionSection()
    ListOperatorsHelper.add(left, to: section1)
    let section2 = CollectionSection()
    ListOperatorsHelper.add(right, to: section2)
    let form = CollectionForm()
    ListOperatorsHelper.add(section1, to: form)
    ListOperatorsHelper.add(section2, to: form)
    return form
}

// MARK:- +++!
/**
 添加 Section 到 CollectionForm, 并通知到代理
 
 - parameter left:  form
 - parameter right: 需要添加的 section
 
 - returns: 添加后的 form
 */
@discardableResult
public func +++! (left: CollectionForm, right: CollectionSection) -> CollectionForm {
    return ListOperatorsHelper.addAndUpdate(right, to: left)
}
/**
 添加 Row 到 Form 的最后一个 Section, 并通知到代理
 
 - parameter left:  form
 - parameter right: row
 */
@discardableResult
public func +++! (left: CollectionForm, right: CollectionItem) -> CollectionForm {
    if let section = left.allSections.last as? CollectionSection {
        ListOperatorsHelper.addAndUpdate(right, to: section)
    } else {
        let section = CollectionSection()
        ListOperatorsHelper.add(right, to: section)
        ListOperatorsHelper.addAndUpdate(section, to: left)
    }
    return left
}

// MARK:- <<<
/**
 添加Row到Section
 
 - parameter left:  section
 - parameter right: row
 
 - returns: section
 */
@discardableResult
public func <<< (left: CollectionSection, right: CollectionItem) -> CollectionSection {
    return ListOperatorsHelper.add(right, to: left)
}

/**
 两个Row创建Section
 
 - parameter left:  第一个 row
 - parameter right: 第二个 row
 
 - returns: 创建好的 section
 */
@discardableResult
public func <<< (left: CollectionItem, right: CollectionItem) -> CollectionSection {
    let section = CollectionSection()
    ListOperatorsHelper.add(left, to: section)
    ListOperatorsHelper.add(right, to: section)
    return section
}

// MARK:- <<<!
/**
 添加Row到Section, 并通知到代理
 使用 <<<!前，
 ***如果section已经被添加到form中***，请确认collectionView已经刷新过（section的信息已经在collectionView上，否则会闪退）
 ***如果section还没有被添加到form中***，就没关系
 
 - parameter left:  section
 - parameter right: row
 
 - returns: section
 */
@discardableResult
public func <<<! (left: CollectionSection, right: CollectionItem) -> CollectionSection {
    return ListOperatorsHelper.addAndUpdate(right, to: left)
}

/**
 添加Row数组到Section, 并通知到代理
 使用 <<<!前，
 ***如果section已经被添加到form中***，请确认collectionView已经刷新过（section的信息已经在collectionView上，否则会闪退）
 ***如果section还没有被添加到form中***，就没关系
 
 - parameter left:  section
 - parameter right: row
 
 - returns: section
 */
@discardableResult
public func <<<! (left: CollectionSection, right: [CollectionItem]) -> CollectionSection {
    return ListOperatorsHelper.addAndUpdate(right, to: left)
}

// MARK:- >>>
/**
 替换Form的所有Section, 并通知到代理

- parameter left:  form
- parameter right: 要替换的Section数组

- returns: form
*/
@discardableResult
public func >>>(left: CollectionForm, right: [CollectionSection]) -> CollectionForm {
    let oldSections = left.allSections
    left.replaceSubrange(0 ..< oldSections.count, with: right)
    guard let handler = left.delegate as? CollectionViewHandler else {
        return left
    }
    handler.reloadCollection()
    return left
}
/**
 替换Section数组到指定范围, 并通知到代理

- parameter left:  form
- parameter right: 元组，( 范围 ，要替换的Section数组 )

- returns: form
*/
@discardableResult
public func >>>(left: CollectionForm, right: (Range<Int>, [CollectionSection])) -> CollectionForm {
    left.replaceSubrange(right.0, with: right.1)
    guard let handler = left.delegate as? CollectionViewHandler else {
        return left
    }
    handler.reloadCollection()
    return left
}
/**
 替换Section的所有Row，并通知到代理

- parameter left:  section
- parameter right: 要替换的Row数组

- returns: section
*/
@discardableResult
public func >>>(left: CollectionSection, newRows: [CollectionItem]) -> CollectionSection {
    return ListOperatorsHelper.replace(newRows, to: left, useAnimation: false)
}
/**
 替换Row数组到指定范围, 并通知到代理

- parameter left:  section
- parameter right: 元组，( 范围 ，要替换的Row数组 )

- returns: section
*/
@discardableResult
public func >>>(left: CollectionSection, right: (Range<Int>, [CollectionItem])) -> CollectionSection {
    left.replaceSubrange(right.0, with: right.1)
    guard let handler = left.form?.delegate as? CollectionViewHandler else {
        return left
    }
    handler.reloadCollection()
    return left
}

/**
 添加 Row 的集合到 CollectionSection
 
 - parameter lhs: section
 - parameter rhs: rows 的集合
 */
public func += <C: Collection>(lhs: inout CollectionSection, rhs: C) where C.Iterator.Element == BaseRow {
    lhs.append(contentsOf: rhs)
}

/**
 添加 Section 的集合到 CollectionForm
 
 - parameter lhs: form
 - parameter rhs: sections 的集合
 */
public func += <C: Collection>(lhs: inout CollectionForm, rhs: C) where C.Iterator.Element == BaseSection {
    lhs.append(contentsOf: rhs)
}
