# GZCTimerHelper

统一的计时器管理帮助类

## 使用方式

### 1、快速使用:

Swift：

```
import GZCTimerHelper

class XXX: GZCTimerHelperDelegate {
    /// 回调方法（每0.5秒执行一次）
    func timerImplement() {
        print(name ?? "")
    }
}

/// 添加监听
let xxx: XXX = ...
GZCTimerHelper.shared.addObserver(xxx)

/// TimerHelper是weak方式引用，xxx释放时会自动移除，也可以手动移除：
GZCTimerHelper.shared.removeObserver(xxx)
```

OC：
```
#import <GZCTimerHelper-Swift.h>

/// 添加
[[GZCTimerHelper shared] addObserver:xxx];
/// 移除
[[GZCTimerHelper shared] removeObserver:xxx];
```

### 2、配置统一的调用时间间隔

可在AppDelegate中配置统一间隔时间：

```
// 配置计时器的间隔时间
GZCTimerHelper.shared.timeInterval = 1.0
```

### 3、不使用单例创建独立的Helper（虽然支持，但尽量少用）

支持创建新的GZCTimerHelper对象（与单例互不干扰）：

```
GZCTimerHelper(3).addObserver(keepModel)
```

## 注意
本框架采用了多线程的回调方式，且最多线程数为10条，因此当10条线程都被堵塞时，会出现回调不及时的情况，因此 **`timerImplement()`方法中请不要包含太多耗时操作！** (认真脸)

## 安装

GZCTimerHelper支持[CocoaPods](https://cocoapods.org)安装：
添加仓库到Podfile:
```
pod 'GZCTimerHelper'
```
执行`pod install`即可安装使用

##  注意：
OC使用Pod方式导入时，必须使用`use_frameworks!`, 如果使用了无法install，在podfile中添加下面代码再试试看：
```
pre_install do |installer| Pod::Installer::Xcode::TargetValidator.send(:define_method, :verify_no_static_framework_transitive_dependencies) {}
end
```


## Author

Guo ZhongCheng, gzhongcheng@qq.com

## License

GZCTimerHelper is available under the MIT license. See the LICENSE file for more info.
